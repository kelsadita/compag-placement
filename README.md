# Compag-Placement
---
A website for campus recruitment in college for collecting and managing the academic information and personal data of students. 


### Deployment checklist ###

* Replace the *baseurl* constant in header.inc.php
* Replace the logout redirect URL.
* Check the mail function's "From:" part under login/fpassword.php
* Change the excel download path.