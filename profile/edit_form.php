<?php 	
	include '../includes/header.inc.php';
	include '../includes/connect.inc.php';
	include '../includes/session.inc.php';
	include '../includes/is_int.inc.php';
?>
<style>
	form input[type = "text"]
	{
		width: 60px;
	}
</style>
<div class="linkcontent" style="margin-top: 60px; width: 750px;">
<?php
	if(isset($_GET['user_id']) && isset($_GET['bref'])){
		
		$user_id = $_GET['user_id'];
		$bref = $_GET['bref'];
		$back_url = baseurl."admin/records.php?ref=".$bref;
		switch($bref){
		
		case '1':
			$branch = "Computer";
			break;
		case '2':
			$branch = "Mechanical";
			break;
		case '3':
			$branch = "EXTC";
			break;
		case '4':
			$branch = "Electrical";
			break;
		case '5':
			$branch = "IT";
			break;
		
		
		}
		
	}else{
		$user_id = $_SESSION['user_id'];
		$branch = $_SESSION['branch'];
		$back_url = "viewprofile.php";		
	}
	
	
	$db=mysqli_connect(DB_HOST,DB_USER,DB_PASSWORD,DB_NAME);
	$bquery="select * from outof where branch='".$branch."'";
	$bdata=mysqli_query($db,$bquery);
	$brow=mysqli_fetch_array($bdata);
	$query="select * from marks where user_id=".$user_id;
	$data=mysqli_query($db,$query);
	$row=mysqli_fetch_array($data);
	if(isset($_POST['submit']))
	{
		extract($_POST);
		
		$sscmk=mysqli_real_escape_string($db,trim($sscmk));
		$sscout=mysqli_real_escape_string($db,trim($sscout));
		if($sscout==0)
		{
			echo "<p class='notify'>Enter proper marks..</p><br /><br/>";
			echo "<center><a href='edit_form.php' class='orange' style = 'text-decoration: none;'>Back</a></center>";
			exit;
		}
		$sscper=($sscmk/$sscout)*100;
		$sscyear=mysqli_real_escape_string($db,trim($sscyear));

		if($row['dipfmk']!=0 && $row['hscmk'] == 0)
		{
			$dipbranch=mysqli_real_escape_string($db,trim($dipbranch));
			$dipfmk=mysqli_real_escape_string($db,trim($dipfmk));
			$dipfout=mysqli_real_escape_string($db,trim($dipfout));
			if($dipfout==0)
			{
				echo "<p class='notify'>Enter proper marks..</p><br /><br/>";
				echo "<center><a href='edit_form.php' class='orange' style = 'text-decoration: none;'>Back</a></center>";
				exit;
			}
			$dipfper=($dipfmk/$dipfout)*100;
			$dipfper=substr($dipfper,0,6);
			$dipyear=mysqli_real_escape_string($db,trim($dipyear));
			$dipaggmk=mysqli_real_escape_string($db,trim($dipaggmk));
			$dipaggout=mysqli_real_escape_string($db,trim($dipaggout));
			if($dipaggout==0)
			{
				echo "<p class='notify'>Enter proper marks..</p><br /><br/>";
				echo "<center><a href='edit_form.php' class='orange' style = 'text-decoration: none;'>Back</a></center>";
				exit;
			}
			$dipaggper=($dipaggmk/$dipaggout)*100;
			$dipaggper=substr($dipaggper,0,6);			
			if($sscyear>=$dipyear)
			{
				echo "<p class='notify'>Problem in SSC and Diploma years...</p><br /><br/>";
				echo "<center><a href='edit_form.php' class='orange' style = 'text-decoration: none;'>Back</a></center>";
				exit;
			}
		}

		elseif($row['hscmk']!=0 && $row['dipfmk'] == 0)
		{
			$hscmk=mysqli_real_escape_string($db,trim($hscmk));
			$hscout=mysqli_real_escape_string($db,trim($hscout));
			if($hscout==0)
			{
				echo "<p class='notify'>Enter proper marks..</p><br /><br/>";
				echo "<center><a href='edit_form.php' class='orange' style = 'text-decoration: none;'>Back</a></center>";
				exit;
			}
			$hscper=($hscmk/$hscout)*100;	
			$hscper=substr($hscper,0,6);
			$hscyear=mysqli_real_escape_string($db,trim($hscyear));
			$sem1mk=mysqli_real_escape_string($db,trim($sem1mk));
			$sem1out=mysqli_real_escape_string($db,trim($sem1out));
			$sem1per=($sem1mk/$sem1out)*100;
			$sem2mk=mysqli_real_escape_string($db,trim($sem2mk));
			$sem2out=mysqli_real_escape_string($db,trim($sem2out));
			$sem2per=($sem2mk/$sem2out)*100;
			if($sscyear>=$hscyear)
			{
				echo "<p class='notify'>Problem in SSC and HSC years...</p><br /><br/>";
				echo "<center><a href='edit_form.php' class='orange' style = 'text-decoration: none;'>Back</a></center>";
				exit;
			}
		}

		elseif($row['hscmk']!=0 && $row['dipfmk'] != 0)
		{
			$hscmk=mysqli_real_escape_string($db,trim($hscmk));
			$hscout=mysqli_real_escape_string($db,trim($hscout));
			if($hscout==0)
			{
				echo "<p class='notify'>Enter proper marks..</p><br /><br/>";
				echo "<center><a href='edit_form.php' class='orange' style = 'text-decoration: none;'>Back</a></center>";
				exit;
			}
			$hscper=($hscmk/$hscout)*100;	
			$hscper=substr($hscper,0,6);
			$hscyear=mysqli_real_escape_string($db,trim($hscyear));
			
			$dipbranch=mysqli_real_escape_string($db,trim($dipbranch));
			$dipfmk=mysqli_real_escape_string($db,trim($dipfmk));
			$dipfout=mysqli_real_escape_string($db,trim($dipfout));
			if($dipfout==0)
			{
				echo "<p class='notify'>Enter proper marks..</p><br /><br/>";
				echo "<center><a href='edit_form.php' class='orange' style = 'text-decoration: none;'>Back</a></center>";
				exit;
			}
			$dipfper=($dipfmk/$dipfout)*100;
			$dipfper=substr($dipfper,0,6);
			$dipyear=mysqli_real_escape_string($db,trim($dipyear));
			$dipaggmk=mysqli_real_escape_string($db,trim($dipaggmk));
			$dipaggout=mysqli_real_escape_string($db,trim($dipaggout));
			if($dipaggout==0)
			{
				echo "<p class='notify'>Enter proper marks..</p><br /><br/>";
				echo "<center><a href='edit_form.php' class='orange' style = 'text-decoration: none;'>Back</a></center>";
				exit;
			}
			$dipaggper=($dipaggmk/$dipaggout)*100;
			$dipaggper=substr($dipaggper,0,6);			
			if($sscyear>=$dipyear)
			{
				echo "<p class='notify'>Problem in SSC and Diploma years...</p><br /><br/>";
				echo "<center><a href='edit_form.php' class='orange' style = 'text-decoration: none;'>Back</a></center>";
				exit;
			}

			if($sscyear>=$hscyear)
			{
				echo "<p class='notify'>Problem in SSC and HSC years...</p><br /><br/>";
				echo "<center><a href='edit_form.php' class='orange' style = 'text-decoration: none;'>Back</a></center>";
				exit;
			}
		}

		

		$cetmk = mysqli_real_escape_string($db,trim($cetmk));
		$cetout = mysqli_real_escape_string($db,trim($cetout));

		if($cetmk == "" || $cetout == "")
		{
			$cetmk = 0;
			$cetout = 0;
		}	

		$aieeemk = mysqli_real_escape_string($db,trim($aieeemk));
		$aieeeout = mysqli_real_escape_string($db,trim($aieeeout));

		if($aieeemk == "" || $aieeeout == "")
		{
			$aieeemk = 0;
			$aieeeout = 0;
		}	

		$sem3mk=mysqli_real_escape_string($db,trim($sem3mk));
		$sem3out=mysqli_real_escape_string($db,trim($sem3out));
		$sem3per=($sem3mk/$sem3out)*100;
		$sem4mk=mysqli_real_escape_string($db,trim($sem4mk));
		$sem4out=mysqli_real_escape_string($db,trim($sem4out));
		$sem4per=($sem4mk/$sem4out)*100;
		$sem5mk=mysqli_real_escape_string($db,trim($sem5mk));
		$sem5out=mysqli_real_escape_string($db,trim($sem5out));
		$sem5per=($sem5mk/$sem5out)*100;
		$sem6mk=mysqli_real_escape_string($db,trim($sem6mk));
		$sem6out=mysqli_real_escape_string($db,trim($sem6out));
		$sem6per=($sem6mk/$sem6out)*100;
		$sem6per=substr($sem6per,0,6);
		if($row['dipfmk']!=0)
		{
			$aggmk=$sem3mk+$sem4mk+$sem5mk+$sem6mk;
			$aggout=$sem3out+$sem4out+$sem5out+$sem6out;
		}
		else 
		{
			$aggmk=$sem1mk+$sem2mk+$sem3mk+$sem4mk+$sem5mk+$sem6mk;
			$aggout=$sem1out+$sem2out+$sem3out+$sem4out+$sem5out+$sem6out;
			$sem1per=substr($sem1per,0,6);
			$sem2per=substr($sem2per,0,6);
		}
		$finalagg=(($aggmk)/($aggout))*100;
		$sscper=substr($sscper,0,6);
		$sem3per=substr($sem3per,0,6);
		$sem4per=substr($sem4per,0,6);
		$sem5per=substr($sem5per,0,6);
		$sem6per=substr($sem6per,0,6);
		$finalagg=substr($finalagg,0,6);
		$live=mysqli_real_escape_string($db,trim($livekt));
		$dead=mysqli_real_escape_string($db,trim($deadkt));
		
		$query="..";
		$bool=0;
		if($row['dipfmk']!=0 && $row['hscmk']==0)
		{
			if(!int($sscmk)||!int($dipfmk)||!int($dipaggmk)||!int($sem3mk)||!int($sem4mk)||!int($sem5mk)||!int($sem6mk))
			{
				echo '<p class="notify">Enter integer values for marks..</p>';
				echo "<center><a href='edit_form.php' class='orange' style = 'text-decoration: none;'>Back</a></center>";
				exit;
			}
			if($sscper>100||$dipfper>100||$dipaggper>100||$sem3per>100||$sem4per>100||$sem5per>100||$sem6per>100||$sscper<=0||$dipfper<=0||$dipaggper<=0||$sem3per<=0||$sem4per<=0||$sem5per<=0||$sem6per<=0||$sscout==0||$dipfout==0||$dipaggout==0)
			{
				echo "<p class='notify'>Enter proper marks..</p><br /><br/>";
				$bool=1;
				echo "<center><a href='edit_form.php' class='orange' style = 'text-decoration: none;'>Back</a></center>";
				exit;
			}
		}

		elseif($row['dipfmk']==0&&$row['hscmk']!=0)
		{
			if(!int($sscmk)||!int($hscmk)||!int($sem1mk)||!int($sem2mk)||!int($sem3mk)||!int($sem4mk)||!int($sem5mk)||!int($sem6mk))
			{
				echo '<p class="notify">Enter integer values for marks..</p>';
				echo "<center><a href='edit_form.php' class='orange' style = 'text-decoration: none;'>Back</a></center>";
				exit;	
			}
			if($sscper>100||$hscper>100||$sem1per>100||$sem2per>100||$sem3per>100||$sem4per>100||$sem5per>100||$sem6per>100||$sscper<=0||$hscper<=0||$sem1per<=0||$sem2per<=0||$sem3per<=0||$sem4per<=0||$sem5per<=0||$sem6per<=0||$sscout==0||$hscout==0)
			{
				echo "<p class='notify'>Enter proper marks..</p><br /><br />";
				$bool=1;
				echo "<center><a href='edit_form.php' class='orange' style = 'text-decoration: none;'>Back</a></center>";
				exit;
			}
		}

		elseif($row['dipfmk']!=0 && $row['hscmk']!=0)
		{
			if(!int($sscmk)||!int($hscmk)||!int($dipfmk)||!int($dipaggmk)||!int($sem3mk)||!int($sem4mk)||!int($sem5mk)||!int($sem6mk))
			{
				echo '<p class="notify">Enter integer values for marks..</p>';
				echo "<center><a href='edit_form.php' class='orange' style = 'text-decoration: none;'>Back</a></center>";
				exit;	
			}
			if($sscper>100||$hscper>100||$dipfper>100||$dipaggper>100||$sem3per>100||$sem4per>100||$sem5per>100||$sem6per>100||$sscper<=0||$hscper<=0||$dipfper<=0||$dipaggper<=0||$sem3per<=0||$sem4per<=0||$sem5per<=0||$sem6per<=0||$sscout==0||$hscout==0)
			{
				echo "<p class='notify'>Enter proper marks..</p><br /><br />";
				$bool=1;
				echo "<center><a href='edit_form.php' class='orange' style = 'text-decoration: none;'>Back</a></center>";
				exit;
			}
		}

		if($row['dipfmk']==0 && $row['hscmk'] != 0 && $bool==0)
		{
			/*$query="update marks set sscmk=$sscmk,sscout=$sscout,sscyear=$sscyear,sscper=$sscper,hscmk=$hscmk,hscout=$hscout,hscyear=$hscyear,hscper=$hscper,
			sem1mk=$sem1mk,sem1out=$sem1out,sem1per=$sem1per,sem2mk=$sem2mk,sem2out=$sem2out,sem2per=$sem2per,sem3mk=$sem3mk,sem3out=$sem3out,sem3per=$sem3per,sem4mk=$sem4mk,sem4out=$sem4out,sem4per=$sem4per,
			sem5mk=$sem5mk,sem5out=$sem5out,sem5per=$sem5per,sem6mk=$sem6mk,sem6out=$sem6out,sem6per=$sem6per,aggmk=$aggmk,aggout=$aggout,finalagg=$finalagg,livekt=$livekt,deadkt=$deadkt 
			where user_id=".$_SESSION['user_id'] ;*/
			$query="update marks set sscmk=$sscmk,sscout=$sscout,sscyear=$sscyear,sscper=$sscper,cetmk=$cetmk, cetout=$cetout, aieeemk=$aieeemk, aieeeout=$aieeeout, hscmk=$hscmk,hscout=$hscout,hscyear=$hscyear,hscper=$hscper,
			sem1mk=$sem1mk,sem1out=$sem1out,sem1per=$sem1per,sem2mk=$sem2mk,sem2out=$sem2out,sem2per=$sem2per,sem3mk=$sem3mk,sem3out=$sem3out,sem3per=$sem3per,sem4mk=$sem4mk,sem4out=$sem4out,sem4per=$sem4per,
			sem5mk=$sem5mk,sem5out=$sem5out,sem5per=$sem5per,sem6mk=$sem6mk,sem6out=$sem6out,sem6per=$sem6per,aggmk=$aggmk,aggout=$aggout,finalagg=$finalagg,livekt=$livekt,deadkt=$deadkt 
			where user_id=".$user_id ;
			mysqli_query($db,$query);
		}
		elseif($row['dipfmk'] !=0 && $row['hscmk'] == 0 &&$bool==0)/*if($row['dipfmk']==1&&$bool==0&&!empty($sscmk)&&!empty($sem3mk)&&!empty($sem4mk)&&!empty($sem5mk))*/
		{
			/*$query="update marks set sscmk=$sscmk,sscout=$sscout,sscyear=$sscyear,sscper=$sscper,dipfmk=$dipfmk,dipfout=$dipfout,dipfper=$dipfper,dipyear=$dipyear,
			dipaggmk=$dipaggmk,dipaggout=$dipaggout,dipaggper=$dipaggout,dipbranch='$dipbranch',
			sem3mk=$sem3mk,sem3out=$sem3out,sem3per=$sem3per,sem4mk=$sem4mk,sem4out=$sem4out,sem4per=$sem4per,
			sem5mk=$sem5mk,sem5out=$sem5out,sem5per=$sem5per,aggmk=$aggmk,aggout=$aggout,finalagg=$finalagg,livekt=$livekt,deadkt=$deadkt 
			where user_id=".$_SESSION['user_id'] ;*/
			$query="update marks set sscmk=$sscmk,sscout=$sscout,sscyear=$sscyear,sscper=$sscper,cetmk=$cetmk, cetout=$cetout, aieeemk=$aieeemk, aieeeout=$aieeeout,dipfmk=$dipfmk,dipfout=$dipfout,dipfper=$dipfper,dipyear=$dipyear,
			dipaggmk=$dipaggmk,dipaggout=$dipaggout,dipaggper=$dipaggper,dipbranch='$dipbranch',
			sem3mk=$sem3mk,sem3out=$sem3out,sem3per=$sem3per,sem4mk=$sem4mk,sem4out=$sem4out,sem4per=$sem4per,
			sem5mk=$sem5mk,sem5out=$sem5out,sem5per=$sem5per,sem6mk=$sem6mk,sem6out=$sem6out,sem6per=$sem6per,aggmk=$aggmk,aggout=$aggout,finalagg=$finalagg,livekt=$livekt,deadkt=$deadkt 
			where user_id=".$user_id ;
			mysqli_query($db,$query);
		}

		elseif($row['dipfmk'] !=0 && $row['hscmk'] != 0 &&$bool==0)
		{
			/*$query="update marks set sscmk=$sscmk,sscout=$sscout,sscyear=$sscyear,sscper=$sscper,dipfmk=$dipfmk,dipfout=$dipfout,dipfper=$dipfper,dipyear=$dipyear,
			dipaggmk=$dipaggmk,dipaggout=$dipaggout,dipaggper=$dipaggout,dipbranch='$dipbranch',
			sem3mk=$sem3mk,sem3out=$sem3out,sem3per=$sem3per,sem4mk=$sem4mk,sem4out=$sem4out,sem4per=$sem4per,
			sem5mk=$sem5mk,sem5out=$sem5out,sem5per=$sem5per,aggmk=$aggmk,aggout=$aggout,finalagg=$finalagg,livekt=$livekt,deadkt=$deadkt 
			where user_id=".$_SESSION['user_id'] ;*/
			$query="update marks set sscmk=$sscmk,sscout=$sscout,sscyear=$sscyear,sscper=$sscper,cetmk=$cetmk, cetout=$cetout, aieeemk=$aieeemk, aieeeout=$aieeeout,hscmk=$hscmk,hscout=$hscout,hscyear=$hscyear,hscper=$hscper,dipfmk=$dipfmk,dipfout=$dipfout,dipfper=$dipfper,dipyear=$dipyear,
			dipaggmk=$dipaggmk,dipaggout=$dipaggout,dipaggper=$dipaggper,dipbranch='$dipbranch',
			sem3mk=$sem3mk,sem3out=$sem3out,sem3per=$sem3per,sem4mk=$sem4mk,sem4out=$sem4out,sem4per=$sem4per,
			sem5mk=$sem5mk,sem5out=$sem5out,sem5per=$sem5per,sem6mk=$sem6mk,sem6out=$sem6out,sem6per=$sem6per,aggmk=$aggmk,aggout=$aggout,finalagg=$finalagg,livekt=$livekt,deadkt=$deadkt 
			where user_id=".$user_id ;
			mysqli_query($db,$query);
		}
		/*else
		{
			echo "DB error..";
			exit;
		}*/
		$_SESSION['update']=1;
		header("location:".$back_url);
		
		//checking whether from admin or from user itself
		
		if(isset($_GET['user_id'])){
		
			$redirect = $_SERVER['PHP_SELF']."user_id=".$user_id."&bref=".$bref;	
			echo $redirect;
		}else{
			$redirect = $_SERVER['PHP_SELF'];
		}
	
	}
		
	//checking whether from admin or from user itself
		
	if(isset($_GET['user_id'])){
	
		$redirect = $_SERVER['PHP_SELF']."?user_id=".$user_id."&bref=".$bref;	
		echo $redirect;
	}else{
		$redirect = $_SERVER['PHP_SELF'];
	}
?>
	<form class="cmxform" id="form" action="<?php echo $redirect;?>" method="post">
		<hr noshade style = "border : 2px solid #CCCCCC;"/>
		<h2>Marks Details<a href="<?=$back_url;?>" class="orange" style = "float: right; font-size: 13px;text-decoration: none;">Back</a></h2>
		<hr noshade style = "border : 2px solid #CCCCCC;"/>
		<br /><br />
		<p id= "heading">SSC/CBSE/ICSE/OTHER</p>
		<label>Marks</label>
	 	<input type="text" name="sscmk" class="required number" value="<?php echo $row['sscmk'];?>"/>&nbsp;/&nbsp;
		<input type="text" name="sscout" class="required number" value="<?php echo $row['sscout'];?>"/><br />
		<label>Year of Passing</label>
		<select id="select" name="sscyear">
		<?php 
			for($i=2000;$i<=2012;$i++)
			{
				?>
					<option <?php if($i==$row['sscyear']) echo 'selected="selected"';?>><?php echo $i;?></option>
				<?php
			}
		?>
		</select><br />

			<p id= "heading">CET and AIEEE</p>
			<label>CET Marks</label>
		 	<input type="text" name="cetmk" value="<?php echo $row['cetmk'];?>"/>&nbsp;/&nbsp;
			<input type="text" name="cetout" value="<?php echo $row['cetout'];?>"/><br />

			<label>AIEEE Marks</label>
		 	<input type="text" name="aieeemk" value="<?php echo $row['aieeemk'];?>"/>&nbsp;/&nbsp;
			<input type="text" name="aieeeout" value="<?php echo $row['aieeeout'];?>"/><br />

		<?php
			if($row['dipfmk']!=0 && $row['hscmk'] == 0)
			{
				?>
				<p id="heading">Diploma</p><br>
				<label>Branch</label>
				<select name="dipbranch">
					<option <?php if($row['dipbranch']=="Computer") echo 'selected="selected"'?>>Computer</option>
					<option <?php if($row['dipbranch']=="IT") echo 'selected="selected"'?>>IT</option>
					<option <?php if($row['dipbranch']=="EXTC") echo 'selected="selected"'?>>EXTC</option>
					<option <?php if($row['dipbranch']=="Electrical") echo 'selected="selected"'?>>Electrical</option>
					<option <?php if($row['dipbranch']=="Mechanical") echo 'selected="selected"'?>>Mechanical</option>
					<option <?php if($row['dipbranch']=="Electronics") echo 'selected="selected"'?>>Electronics</option>
				</select><br />
				<label>Final Year Marks</label>
				<input type="text" name="dipfmk" class="required number" value="<?php echo $row['dipfmk'];?>"/>&nbsp;/&nbsp;
				<input type="text" name="dipfout" class="required number" value="<?php echo $row['dipfout'];?>"/><br />
				<label>Year of Passing</label>
				<select id="select" name="dipyear">
					<?php 
						for($i=2000;$i<=2012;$i++)
						{
							?>
								<option <?php if($i==$row['dipyear']) echo 'selected="selected"';?>><?php echo $i;?></option>
							<?php
						}
					?>
				</select><br />
				<label>Aggregate Marks</label>
				<input type="text" name="dipaggmk" class="required number" value="<?php echo $row['dipaggmk'];?>"/>&nbsp;/&nbsp;
				<input type="text" name="dipaggout" class="required number" value="<?php echo $row['dipaggout'];?>"/><br />
				<?php
			}	
			elseif($row['dipfmk'] == 0 && $row['hscmk'] != 0)
			{
				?>
				<p id="heading">HSC</p><br>
				<label>Marks</label>
				<input type="text" name="hscmk" class="required number" value="<?php echo $row['hscmk'];?>"/>&nbsp;/&nbsp;
				<input type="text" name="hscout" class="required number" value="<?php echo $row['hscout'];?>"/><br />
				<label>Year of Passing</label>
				<select id="select" name="hscyear">
				<?php 
					for($i=2000;$i<=2012;$i++)
					{
						?>
							<option <?php if($i==$row['hscyear']) echo 'selected="selected"';?>><?php echo $i;?></option>
						<?php
					}
				?>
				</select><br />
				<p id= "heading">SEM 1</p>
				<label>Marks</label>
				<input type="text" name="sem1mk" class="required number" value="<?php echo $row['sem1mk'];?>"/>&nbsp;/&nbsp;
				<input type="text" name="sem1out" class="required number" readonly="readonly" value="<?php echo $brow['sem1'];?>"/>
				<p id= "heading">SEM 2</p>
				<label>Marks</label>
				<input type="text" name="sem2mk" class="required number" value="<?php echo $row['sem2mk'];?>"/>&nbsp;/&nbsp;
				<input type="text" name="sem2out" class="required number" readonly="readonly" value="<?php echo $brow['sem2'];?>"/>
				<?php	
			}

			elseif($row['dipfmk'] != 0 && $row['hscmk'] != 0)
			{
				?>

				<p id="heading">HSC</p><br>
				<label>Marks</label>
				<input type="text" name="hscmk" class="required number" value="<?php echo $row['hscmk'];?>"/>&nbsp;/&nbsp;
				<input type="text" name="hscout" class="required number" value="<?php echo $row['hscout'];?>"/><br />
				<label>Year of Passing</label>
				<select id="select" name="hscyear">
				<?php 
					for($i=2000;$i<=2012;$i++)
					{
						?>
							<option <?php if($i==$row['hscyear']) echo 'selected="selected"';?>><?php echo $i;?></option>
						<?php
					}
				?>
				</select><br />
				

				<p id="heading">Diploma</p><br>
				<label>Branch</label>
				<select name="dipbranch">
					<option <?php if($row['dipbranch']=="Computer") echo 'selected="selected"'?>>Computer</option>
					<option <?php if($row['dipbranch']=="IT") echo 'selected="selected"'?>>IT</option>
					<option <?php if($row['dipbranch']=="EXTC") echo 'selected="selected"'?>>EXTC</option>
					<option <?php if($row['dipbranch']=="Electrical") echo 'selected="selected"'?>>Electrical</option>
					<option <?php if($row['dipbranch']=="Mechanical") echo 'selected="selected"'?>>Mechanical</option>
					<option <?php if($row['dipbranch']=="Electronics") echo 'selected="selected"'?>>Electronics</option>
				</select><br />
				<label>Final Year Marks</label>
				<input type="text" name="dipfmk" class="required number" value="<?php echo $row['dipfmk'];?>"/>&nbsp;/&nbsp;
				<input type="text" name="dipfout" class="required number" value="<?php echo $row['dipfout'];?>"/><br />
				<label>Year of Passing</label>
				<select id="select" name="dipyear">
					<?php 
						for($i=2000;$i<=2012;$i++)
						{
							?>
								<option <?php if($i==$row['dipyear']) echo 'selected="selected"';?>><?php echo $i;?></option>
							<?php
						}
					?>
				</select><br />
				<label>Aggregate Marks</label>
				<input type="text" name="dipaggmk" class="required number" value="<?php echo $row['dipaggmk'];?>"/>&nbsp;/&nbsp;
				<input type="text" name="dipaggout" class="required number" value="<?php echo $row['dipaggout'];?>"/><br />
				<?php
			}
		?>	
		<p id= "heading">SEM 3</p>
		<label>Marks</label>
		<input type="text" name="sem3mk" class="required number" value="<?php echo $row['sem3mk'];?>"/>&nbsp;/&nbsp;
		<input type="text" name="sem3out" class="required number" readonly="readonly" value="<?php echo $brow['sem3'];?>"/>
		<p id= "heading">SEM 4</p>
		<label>Marks</label>
		<input type="text" name="sem4mk" class="required number" value="<?php echo $row['sem4mk'];?>"/>&nbsp;/&nbsp;
		<input type="text" name="sem4out" class="required number" readonly="readonly" value="<?php echo $brow['sem4'];?>"/>
		<p id= "heading">SEM 5</p>
		<label>Marks</label>
		<input type="text" name="sem5mk" class="required number" value="<?php echo $row['sem5mk'];?>"/>&nbsp;/&nbsp;
		<input type="text" name="sem5out" class="required number" readonly="readonly" value="<?php echo $brow['sem5'];?>"/>
		<p id= "heading">SEM 6</p>
		<label>Marks</label>
		<input type="text" name="sem6mk" value="<?php echo $row['sem6mk'];?>"/>&nbsp;/&nbsp;
		<input type="text" name="sem6out" class="required number" readonly="readonly" value="<?php echo $brow['sem6'];?>"/>
		<p id= "heading">ATKT</p>
		<label>Live</label>
		<select name="livekt" id="live">
		<?php 
			for($i=0;$i<=10;$i++)
			{
				?>
					<option <?php if($i==$row['livekt']) echo 'selected="selected"';?>><?php echo $i;?></option>
				<?php
			}
		?>
		</select>
		<!--<div id="livekt"></div>-->
		<label>Dead</label>
		<select name="deadkt" id="dead">
		<?php 
			for($i=0;$i<=10;$i++)
			{
				?>
					<option <?php if($i==$row['deadkt']) echo 'selected="selected"';?>><?php echo $i;?></option>
				<?php
			}
		?>
		</select><br /><br />
		<br />
		<label>&nbsp;</label>
		<input type="submit" value="Submit" name="submit" class="button" /></div>
	</form>
</div>
<?php include '../includes/footer.inc.php';?>