<?php
	
	include '../includes/header.inc.php';
	include '../includes/connect.inc.php';
	
	if(isset($_GET['user_id'])){
		
		$user_id = $_GET['user_id'];
		$roll_no = $_GET['roll_no'];
		$ref	 = $_GET['ref'];
		$back_url = baseurl."admin/records.php?ref=".$ref;
		$action_url = $_SERVER['PHP_SELF']."?user_id=".$user_id."&roll_no=".$roll_no."&ref=".$ref;
	}else{
		$user_id = $_SESSION['user_id'];
		$roll_no = $_SESSION['roll_no'];
		$back_url = "viewprofile.php";	
		$action_url = $_SERVER['PHP_SELF'];
	}

	$db = mysqli_connect(DB_HOST, DB_USER, DB_PASSWORD, DB_NAME);

	if (isset( $_POST['submit'] )) {

		$fname  = mysqli_real_escape_string($db, trim($_POST['fname']));
		$mname  = mysqli_real_escape_string($db, trim($_POST['mname']));
		$lname  = mysqli_real_escape_string($db, trim($_POST['lname']));
		$gender = mysqli_real_escape_string($db, trim($_POST['gender']));
		$contact_no = mysqli_real_escape_string($db, trim($_POST['contact_no']));
		$address = mysqli_real_escape_string($db, trim($_POST['address']));
		

		if(!empty($fname) && !empty($lname) && !empty($contact_no) && !empty($address))
		{ 
			if(strlen($contact_no)>10||strlen($contact_no)<10)
			{
				echo "<p class='notify'>Enter proper mobile number..</p><br /><br/>";
				echo "<center><a href='edit_profile.php' class='orange' style = 'text-decoration: none;'>Back</a></center>";
				exit;
			}
			$update_query = "update user set fname = '$fname', mname = '$mname', lname = '$lname', contact_no = $contact_no, address = '$address', gender = '$gender' where roll_no = $roll_no and user_id = $user_id";
			mysqli_query($db, $update_query);
			
			$_SESSION['update']= "Your profile has been updated successfully !";
			header("Location: ".$back_url);
		}
		else
		{ 
			$notice = "You need to add the required information!";
		}
	}
	
	$query = "select * from user where user_id = ".$user_id;
	$data = mysqli_query($db, $query);

	while($personal_data = mysqli_fetch_array($data)){ 
		
		$fname      = $personal_data['fname'];
		$mname      = $personal_data['mname'];
		$lname      = $personal_data['lname'];
		$gender	    = $personal_data['gender'];
		$contact_no = $personal_data['contact_no'];
		$address    = $personal_data['address'];
		$dob        = $personal_data['dob'];
	}
	

?>

<div class="linkcontent" style = "margin-top : 40px; width: 600px;" >
    <?php if(!empty($notice)){ ?>
    <p class="notify"><?=$notice;?></p>
    <?php }?>
    <hr noshade style = "border: 2px solid #CCCCCC;" />
    <h2>Edit Personal Details<a href="<?=$back_url;?>" class="orange" style="float:right; font-size:13px;text-decoration:none;">back</a></h2>
    <hr noshade style = "border: 2px solid #CCCCCC;" />
    <br /><br />
    <form action = "<?=$action_url;?>" method = "post" id = "form">
	<label>First Name : </label>
	<input type="text" name="fname" class="required" value="<?=$fname;?>" /><br />
	<label>Middle Name : </label>
	<input type="text" name="mname" value="<?=$mname;?>" /><br />
	<label>Last Name : </label>
	<input type="text" name="lname" class="required" value="<?=$lname;?>" /><br />
	<label>Gender : </label>
	<select name="gender">
		<option <?php if($gender == "Male") echo "SELECTED" ?>>Male</option>
		<option <?php if($gender == "Female") echo "SELECTED" ?>>Female</option>
	</select><br />
	<label>Contact No. : </label>
	<input type="text" name="contact_no" id="contact_no" class="required number" value="<?=$contact_no;?>" /><br />
	<label>Address : </label>
	<textarea name="address" rows="7" cols="30"><?=$address; ?></textarea><br />
	<label></label>
	<input type="submit" name="submit" value="Update" class="button"/>
   </form>
</div>