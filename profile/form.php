<?php 	
	include '../includes/header.inc.php';
	include '../includes/connect.inc.php';
	include '../includes/is_present.inc.php';
	include '../includes/session.inc.php';
	include '../includes/is_int.inc.php';
	// redirecting admin from user form
	if ($_SESSION['user_id'] ===  '192473') {
		header("Location: ".baseurl."admin/records.php?ref=1");
	}
?>
<style>
	form input[type="text"]{
		
		width: 60px;
	}
</style>
<div class="linkcontent" >
<?php
	$db=mysqli_connect(DB_HOST,DB_USER,DB_PASSWORD,DB_NAME);
	$bquery="select * from outof where branch='".$_SESSION['branch']."'";
	$bdata=mysqli_query($db,$bquery);
	$brow=mysqli_fetch_array($bdata);
	if(isset($_POST['submit']))
	{
		extract($_POST);
		$user_id=$_SESSION['user_id'];
		$query="select * from marks where user_id='$user_id'";
		$data=mysqli_query($db,$query);
		if(mysqli_num_rows($data)!=0)
		{
			echo "<p class='notify'>You have already entered marks, please contact admin..</p>";
			exit;
		}
		$sscmk=mysqli_real_escape_string($db,trim($sscmk));
		$sscout=mysqli_real_escape_string($db,trim($sscout));
		if($sscout==0)
		{
			echo "<p class='notify'>Enter proper marks..</p><br /><br/>";
			echo "<center><a href='form.php' class='orange' style = 'text-decoration: none;'>Back</a></center>";
			exit;
		}
		$sscper=($sscmk/$sscout)*100;
		$sscper=substr($sscper,0,6);
		$sscyear=mysqli_real_escape_string($db,trim($sscyear));
		$hscdip=mysqli_real_escape_string($db,trim($hscdip));
		if($hscdip=='Diploma')
		{
			$dipbranch=mysqli_real_escape_string($db,trim($dipbranch));
			$dipfmk=mysqli_real_escape_string($db,trim($dipfmk));
			$dipfout=mysqli_real_escape_string($db,trim($dipfout));
			if($dipfout==0)
			{
				echo "<p class='notify'>Enter proper marks..</p><br /><br/>";
				echo "<center><a href='form.php' class='orange' style = 'text-decoration: none;'>Back</a></center>";
				exit;
			}
			$dipfper=($dipfmk/$dipfout)*100;
			$dipfper=substr($dipfper,0,6);
			$dipyear=mysqli_real_escape_string($db,trim($dipyear));
			$dipaggmk=mysqli_real_escape_string($db,trim($dipaggmk));
			$dipaggout=mysqli_real_escape_string($db,trim($dipaggout));
			if($dipaggout==0)
			{
				echo "<p class='notify'>Enter proper marks..</p><br /><br/>";
				echo "<center><a href='form.php' class='orange' style = 'text-decoration: none;'>Back</a></center>";
				exit;
			}
			$dipaggper=($dipaggmk/$dipaggout)*100;
			$dipaggper=substr($dipaggper,0,6);
			$dip_hsc=1;
			if($sscyear>=$dipyear)
			{
				echo "<p class='notify'>Problem in SSC and Diploma years...</p><br /><br/>";
				echo "<center><a href='form.php' class='orange' style = 'text-decoration: none;'>Back</a></center>";
				exit;
			}
		}

		elseif($hscdip=='HSC')
		{
			$hscmk=mysqli_real_escape_string($db,trim($hscmk));
			$hscout=mysqli_real_escape_string($db,trim($hscout));
			$hscout=mysqli_real_escape_string($db,trim($hscout));
			if($hscout==0)
			{
				echo "<p class='notify'>Enter proper marks..</p><br /><br/>";
				echo "<center><a href='form.php' class='orange' style = 'text-decoration: none;'>Back</a></center>";
				exit;
			}

			$hscper=($hscmk/$hscout)*100;
			$hscper=substr($hscper,0,6);
			$hscyear=mysqli_real_escape_string($db,trim($hscyear));

			if($sscyear>=$hscyear)
			{
				echo "<p class='notify'>Problem in SSC and HSC years...</p><br /><br/>";
				echo "<center><a href='form.php' class='orange' style = 'text-decoration: none;'>Back</a></center>";
				exit;
			}

			$cetmk = mysqli_real_escape_string($db,trim($cetmk));
			$cetout = mysqli_real_escape_string($db,trim($cetout));

			if($cetmk == "" || $cetout == "")
			{
				$cetmk = 0;
				$cetout = 0;
			}	
			
			$aieeemk = mysqli_real_escape_string($db,trim($aieeemk));
			$aieeeout = mysqli_real_escape_string($db,trim($aieeeout));

			if($aieeemk == "" || $aieeeout == "")
			{
				$aieeemk = 0;
				$aieeeout = 0;
			}	
			
			$sem1mk=mysqli_real_escape_string($db,trim($sem1mk));
			$sem1out=mysqli_real_escape_string($db,trim($sem1out));
			$sem1per=($sem1mk/$sem1out)*100;
			$sem1per=substr($sem1per,0,6);			
			$sem2mk=mysqli_real_escape_string($db,trim($sem2mk));
			$sem2out=mysqli_real_escape_string($db,trim($sem2out));
			$sem2per=($sem2mk/$sem2out)*100;
			$sem2per=substr($sem2per,0,6);
			$dip_hsc=0;
		}

		elseif($hscdip=='Both')
		{
			$dipbranch=mysqli_real_escape_string($db,trim($dipbranch));
			$dipfmk=mysqli_real_escape_string($db,trim($dipfmk));
			$dipfout=mysqli_real_escape_string($db,trim($dipfout));

			if($dipfout==0)
			{
				echo "<p class='notify'>Enter proper marks..</p><br /><br/>";
				echo "<center><a href='form.php' class='orange' style = 'text-decoration: none;'>Back</a></center>";
				exit;
			}

			$dipfper=($dipfmk/$dipfout)*100;
			$dipfper=substr($dipfper,0,6);
			$dipyear=mysqli_real_escape_string($db,trim($dipyear));
			$dipaggmk=mysqli_real_escape_string($db,trim($dipaggmk));
			$dipaggout=mysqli_real_escape_string($db,trim($dipaggout));

			if($dipaggout==0)
			{
				echo "<p class='notify'>Enter proper marks..</p><br /><br/>";
				echo "<center><a href='form.php' class='orange' style = 'text-decoration: none;'>Back</a></center>";
				exit;
			}

			$dipaggper=($dipaggmk/$dipaggout)*100;
			$dipaggper=substr($dipaggper,0,6);
			
			if($sscyear>=$dipyear)
			{
				echo "<p class='notify'>Problem in SSC and Diploma years...</p><br /><br/>";
				echo "<center><a href='form.php' class='orange' style = 'text-decoration: none;'>Back</a></center>";
				exit;
			}

			$hscmk=mysqli_real_escape_string($db,trim($hscmk));
			$hscout=mysqli_real_escape_string($db,trim($hscout));
			$hscout=mysqli_real_escape_string($db,trim($hscout));

			if($hscout==0)
			{
				echo "<p class='notify'>Enter proper marks..</p><br /><br/>";
				echo "<center><a href='form.php' class='orange' style = 'text-decoration: none;'>Back</a></center>";
				exit;
			}

			$hscper=($hscmk/$hscout)*100;
			$hscper=substr($hscper,0,6);
			$hscyear=mysqli_real_escape_string($db,trim($hscyear));

			if($sscyear>=$hscyear)
			{
				echo "<p class='notify'>Problem in SSC and HSC years...</p><br /><br/>";
				echo "<center><a href='form.php' class='orange' style = 'text-decoration: none;'>Back</a></center>";
				exit;
			}

			$cetmk = mysqli_real_escape_string($db,trim($cetmk));
			$cetout = mysqli_real_escape_string($db,trim($cetout));

			if($cetmk == "" || $cetout == "")
			{
				$cetmk = 0;
				$cetout = 0;
			}	
			
			$aieeemk = mysqli_real_escape_string($db,trim($aieeemk));
			$aieeeout = mysqli_real_escape_string($db,trim($aieeeout));

			if($aieeemk == "" || $aieeeout == "")
			{
				$aieeemk = 0;
				$aieeeout = 0;
			}	

			$dip_hsc=2;
		}

		$sem3mk=mysqli_real_escape_string($db,trim($sem3mk));
		$sem3out=mysqli_real_escape_string($db,trim($sem3out));
		$sem3per=($sem3mk/$sem3out)*100;
		$sem3per=substr($sem3per,0,6);		

		$sem4mk=mysqli_real_escape_string($db,trim($sem4mk));
		$sem4out=mysqli_real_escape_string($db,trim($sem4out));
		$sem4per=($sem4mk/$sem4out)*100;
		$sem4per=substr($sem4per,0,6);		

		$sem5mk=mysqli_real_escape_string($db,trim($sem5mk));
		$sem5out=mysqli_real_escape_string($db,trim($sem5out));
		$sem5per=($sem5mk/$sem5out)*100;
		$sem5per=substr($sem5per,0,6);

		$sem6mk=mysqli_real_escape_string($db,trim($sem6mk));
		$sem6out=mysqli_real_escape_string($db,trim($sem6out));
		$sem6per=($sem6mk/$sem6out)*100;
		$sem6per=substr($sem6per,0,6);

		$aggmk=0;
		$aggout=0;

		if($hscdip == 'Diploma' || $hscdip == 'Both')
		{
			$aggmk=$sem3mk+$sem4mk+$sem5mk+$sem6mk;
			$aggout=$sem3out+$sem4out+$sem5out+$sem6out;
		}

		elseif($hscdip == 'HSC') 
		{
			$aggmk=$sem1mk+$sem2mk+$sem3mk+$sem4mk+$sem5mk+$sem6mk;
			$aggout=$sem1out+$sem2out+$sem3out+$sem4out+$sem5out+$sem6out;
		}

		$finalagg=(($aggmk)/($aggout))*100;
		$finalagg=substr($finalagg,0,6);		
		$live=mysqli_real_escape_string($db,trim($livekt));
		$dead=mysqli_real_escape_string($db,trim($deadkt));
		$user_id=$_SESSION['user_id'];
		$query="..";
		$bool=0;

		if($hscdip=='Diploma')
		{
			if(!int($sscmk)||!int($dipfmk)||!int($dipaggmk)||!int($sem3mk)||!int($sem4mk)||!int($sem5mk)||!int($sem6mk))
			{
				echo "Enter integer values for marks......";
				echo "<center><a href='form.php' class='orange' style = 'text-decoration: none;'>Back</a></center>";
				exit;
			}

			if($sscper>100||$dipfper>100||$dipaggper>100||$sem3per>100||$sem4per>100||$sem5per>100||$sem6per>100||$sscper<=0||$dipfper<=0||$dipaggper<=0||$sem3per<=0||$sem4per<=0||$sem5per<=0||$sem6per<=0)
			{
				echo "<p class='notify'>Enter proper marks..</p>";
				$bool=1;
				echo "<center><a href='form.php' class='orange' style = 'text-decoration: none;'>Back</a></center>";
				exit;
			}
		}
		
		elseif($hscdip=='HSC')
		{
			if(!int($sscmk)||!int($hscmk)||!int($sem1mk)||!int($sem2mk)||!int($sem3mk)||!int($sem4mk)||!int($sem5mk))
			{
				echo "Enter integer values for marks..";
				echo "<center><a href='form.php' class='orange' style = 'text-decoration: none;'>Back</a></center>";
				exit;
			}
			if($sscper>100||$hscper>100||$sem1per>100||$sem2per>100||$sem3per>100||$sem4per>100||$sem5per>100||$sem6per>100||$sscper<=0||$hscper<=0||$sem1per<=0||$sem2per<=0||$sem3per<=0||$sem4per<=0||$sem5per<=0||$sem6per<=0)
			{
				echo "<p class='notify'>Enter proper marks..</p>";
				$bool=1;
				echo "<center><a href='form.php' class='orange' style = 'text-decoration: none;'>Back</a></center>";
				exit;
			}
		}

		elseif($hscdip=='Both')
		{
			if(!int($sscmk)||!int($hscmk)||!int($dipfmk)||!int($dipaggmk)||!int($sem3mk)||!int($sem4mk)||!int($sem5mk)||!int($sem6mk))
			{
				echo "Enter integer values for marks..";
				echo "<center><a href='form.php' class='orange' style = 'text-decoration: none;'>Back</a></center>";
				exit;
			}

			if($sscper>100||$hscper>100||$dipfper>100||$dipaggper>100||$sem3per>100||$sem4per>100||$sem5per>100||$sem6per>100||$sscper<=0||$hscper<=0||$dipfper<=0||$dipaggper<=0||$sem3per<=0||$sem4per<=0||$sem5per<=0||$sem6per<=0)
			{
				echo "<p class='notify'>Enter proper marks..</p>";
				$bool=1;
				echo "<center><a href='form.php' class='orange' style = 'text-decoration: none;'>Back</a></center>";
				exit;
			}
		}

		if($hscdip=='HSC' && $bool==0 && !empty($sscmk) && !empty($hscmk) && !empty($sem1mk) && !empty($sem2mk) && !empty($sem3mk) && !empty($sem4mk) && !empty($sem5mk) && !empty($sem6mk))
		{
			$query="insert into marks(user_id,sscmk,sscout,sscyear,sscper,hscmk,hscout,hscyear,hscper,cetmk,cetout,aieeemk,aieeeout,
			sem1mk,sem1out,sem1per,sem2mk,sem2out,sem2per,sem3mk,sem3out,sem3per,sem4mk,sem4out,sem4per,
			sem5mk,sem5out,sem5per,sem6mk,sem6out,sem6per,aggmk,aggout,finalagg,livekt,deadkt)
			values($user_id,$sscmk,$sscout,$sscyear,$sscper,$hscmk,$hscout,$hscyear,$hscper,$cetmk,$cetout,$aieeemk,$aieeeout,
			$sem1mk,$sem1out,$sem1per,$sem2mk,$sem2out,$sem2per,$sem3mk,$sem3out,
			$sem3per,$sem4mk,$sem4out,$sem4per,$sem5mk,$sem5out,$sem5per,$sem6mk,
			$sem6out,$sem6per,$aggmk,$aggout,$finalagg,$livekt,$deadkt)";
			
			/*$query="insert into marks(user_id,sscmk,sscout,sscyear,sscper,hscmk,hscout,hscyear,hscper,
			sem1mk,sem1out,sem1per,sem2mk,sem2out,sem2per,sem3mk,sem3out,sem3per,sem4mk,sem4out,sem4per,
			sem5mk,sem5out,sem5per,aggmk,aggout,finalagg,livekt,deadkt)
			values($user_id,$sscmk,$sscout,$sscyear,$sscper,$hscmk,$hscout,$hscyear,$hscper,
			$sem1mk,$sem1out,$sem1per,$sem2mk,$sem2out,$sem2per,$sem3mk,$sem3out,
			$sem3per,$sem4mk,$sem4out,$sem4per,$sem5mk,$sem5out,$sem5per,$aggmk,$aggout,$finalagg,$livekt,$deadkt)";*/
			mysqli_query($db,$query);
		}

		elseif($hscdip=='Diploma' && $bool==0 && !empty($sscmk) && !empty($dipfmk) && !empty($dipaggmk) && !empty($sem3mk) && !empty($sem4mk) && !empty($sem5mk) && !empty($sem6mk)) 
		{
			$query="insert into marks(user_id,sscmk,sscout,sscyear,sscper,dipfmk,dipfout,dipfper,dipyear,dipaggmk,dipaggout,dipaggper,dipbranch,
			sem3mk,sem3out,sem3per,sem4mk,sem4out,sem4per,sem5mk,sem5out,sem5per,sem6mk,sem6out,sem6per,aggmk,aggout,finalagg,livekt,deadkt)
			values($user_id,$sscmk,$sscout,$sscyear,$sscper,$dipfmk,$dipfout,$dipfper,$dipyear,$dipaggmk,$dipaggout,$dipaggper,'$dipbranch',
			$sem3mk,$sem3out,$sem3per,$sem4mk,$sem4out,$sem4per,$sem5mk,$sem5out,$sem5per,$sem6mk,	$sem6out,$sem6per,$aggmk,$aggout,$finalagg,$livekt,$deadkt)";
			/*$query="insert into marks(user_id,sscmk,sscout,sscyear,sscper,dipfmk,dipfout,dipfper,dipyear,dipaggmk,dipaggout,dipaggper,dipbranch,
			sem3mk,sem3out,sem3per,sem4mk,sem4out,sem4per,sem5mk,sem5out,sem5per,aggmk,aggout,finalagg,livekt,deadkt)
			values($user_id,$sscmk,$sscout,$sscyear,$sscper,$dipfmk,$dipfout,$dipfper,$dipyear,$dipaggmk,$dipaggout,$dipaggper,'$dipbranch',
			$sem3mk,$sem3out,$sem3per,$sem4mk,$sem4out,$sem4per,$sem5mk,$sem5out,$sem5per,$aggmk,$aggout,$finalagg,$livekt,$deadkt)";*/
			mysqli_query($db,$query);
		}

		elseif($hscdip=='Both' && $bool==0 && !empty($sscmk) && !empty($hscmk) && !empty($dipfmk) && !empty($dipaggmk) && !empty($sem3mk) && !empty($sem4mk) && !empty($sem5mk) && !empty($sem6mk))
		{
			$query="insert into marks(user_id,sscmk,sscout,sscyear,sscper,hscmk,hscout,hscyear,hscper,cetmk,cetout,aieeemk,aieeeout,
			dipfmk,dipfout,dipfper,dipyear,dipaggmk,dipaggout,dipaggper,dipbranch,sem3mk,sem3out,sem3per,sem4mk,sem4out,sem4per,
			sem5mk,sem5out,sem5per,sem6mk,sem6out,sem6per,aggmk,aggout,finalagg,livekt,deadkt)
			values($user_id,$sscmk,$sscout,$sscyear,$sscper,$hscmk,$hscout,$hscyear,$hscper,$cetmk,$cetout,$aieeemk,$aieeeout,$dipfmk,$dipfout,$dipfper,$dipyear,$dipaggmk,$dipaggout,$dipaggper,'$dipbranch',$sem3mk,$sem3out,$sem3per,$sem4mk,$sem4out,$sem4per,$sem5mk,$sem5out,$sem5per,$sem6mk,
			$sem6out,$sem6per,$aggmk,$aggout,$finalagg,$livekt,$deadkt)";

			/*$query="insert into marks(user_id,sscmk,sscout,sscyear,sscper,hscmk,hscout,hscyear,hscper,
			sem1mk,sem1out,sem1per,sem2mk,sem2out,sem2per,sem3mk,sem3out,sem3per,sem4mk,sem4out,sem4per,
			sem5mk,sem5out,sem5per,aggmk,aggout,finalagg,livekt,deadkt)
			values($user_id,$sscmk,$sscout,$sscyear,$sscper,$hscmk,$hscout,$hscyear,$hscper,
			$sem1mk,$sem1out,$sem1per,$sem2mk,$sem2out,$sem2per,$sem3mk,$sem3out,
			$sem3per,$sem4mk,$sem4out,$sem4per,$sem5mk,$sem5out,$sem5per,$aggmk,$aggout,$finalagg,$livekt,$deadkt)";*/
			mysqli_query($db,$query);
		}

		else
		{
			echo "DB error..";
			exit;
		}

		$_SESSION['first']=1;
		header("location:".baseurl."profile/viewprofile.php");
	}
?>
	<form class="cmxform" id="form" action="<?php echo $_SERVER['PHP_SELF'];?>" method="post">
		<br /><h2>Marks Details</h2><hr /><br /><br />
		<p id= "heading">SSC/CBSE/ICSE/OTHER</p>
		<label>Marks</label>
		<input type="text" name="sscmk" class="required number"/>&nbsp;/&nbsp;
		<input type="text" name="sscout" class="required number"/><br />
		<label>Year of Passing</label>
		<select id="select" name="sscyear">
		<?php
			for($i=2000;$i<=2012;$i++)
			{
				echo "<option>$i</option>";
			}
		?>
		</select><br />
		<p id= "heading">HSC/Diploma</p>
		<label>Category</label>
		<select name="hscdip" id="main">
    		<option>Select Option</option>
    		<option>HSC</option>
			<option>Diploma</option>
			<option>Both</option>
		</select><br /><br />
		<div id="loadContent"  ></div>
		<div id = "markform" style = "display : none;">
		<h2>Degree</h2>
		<div id = "degree">
			<p id= "heading">SEM 1</p>
			<label>Marks</label>
			<input type="text" name="sem1mk" class="required number"/>&nbsp;/&nbsp;
			<input type="text" name="sem1out" class="required number" readonly="readonly" value="<?php echo $brow['sem1'];?>"/>
			<p id= "heading">SEM 2</p>
			<label>Marks</label>
			<input type="text" name="sem2mk" class="required number"/>&nbsp;/&nbsp;
			<input type="text" name="sem2out" class="required number" readonly="readonly" value="<?php echo $brow['sem2'];?>"/>
		</div>
		<p id= "heading">SEM 3</p>
		<label>Marks</label>
		<input type="text" name="sem3mk" class="required number"/>&nbsp;/&nbsp;
		<input type="text" name="sem3out" class="required number" readonly="readonly" value="<?php echo $brow['sem3'];?>"/>
		<p id= "heading">SEM 4</p>
		<label>Marks</label>
		<input type="text" name="sem4mk" class="required number"/>&nbsp;/&nbsp;
		<input type="text" name="sem4out" class="required number" readonly="readonly" value="<?php echo $brow['sem4'];?>"/>
		<p id= "heading">SEM 5</p>
		<label>Marks</label>
		<input type="text" name="sem5mk" class="required number"/>&nbsp;/&nbsp;
		<input type="text" name="sem5out" class="required number" readonly="readonly" value="<?php echo $brow['sem5'];?>"/>
		<p id= "heading">SEM 6</p>
		<label>Marks</label>
		<input type="text" name="sem6mk" />&nbsp;/&nbsp;
		<input type="text" name="sem6out" class="required number" readonly="readonly" value="<?php echo $brow['sem6'];?>"/>
		<p id= "heading">ATKT</p>
		<label>Live</label>
		<select name="livekt" id="live">
		<?php
			for($i=0;$i<=10;$i++)
			{
				echo "<option>$i</option>";
			}
		?>
		</select>
		<!--<div id="livekt"></div>-->
		<label>Dead</label>
		<select name="deadkt" id="dead">
		<?php
			for($i=0;$i<=10;$i++)
			{
				echo "<option>$i</option>";
			}
		?>
		</select><br /><br />
		<!--<div id="deadkt"></div>-->
		<br />
		<label>&nbsp;</label>
		<input type="submit" value="Submit" name="submit" class="button" /></div>
		</div>
	</form>
<?php include '../includes/footer.inc.php';?>