<?php
if(isset($_GET['type']))
{
	$type=$_GET['type'];
	if($type=='Diploma')
	{
		?>
		<h2>Diploma</h2><hr /><br>
		<label>Branch</label>
		<select name="dipbranch" style="height: 30px;">
			<option>Computer</option>
			<option>IT</option>
			<option>EXTC</option>
			<option>Electrical</option>
			<option>Mechanical</option>
			<option>Electronics</option>
		</select><br /><br />
		<label>Final Year Marks</label>
		<input type="text" name="dipfmk" class="required number"/>&nbsp;/&nbsp;<input type="text" name="dipfout" class="required number"/><br />
		<label>Year of Passing</label>
		<select id="select" name="dipyear">
			<?php
			for($i=2000;$i<=2015;$i++)
			{
				echo "<option>$i</option>";
			}
			?>
		</select><br /><br />
		<label>Aggregate Marks</label>
		<input type="text" name="dipaggmk" class="required number" />&nbsp;/&nbsp;<input type="text" name="dipaggout" class="required number" /><br />
		<?php
	}
	elseif($type=='HSC')
	{
		?>
		<h2>HSC&nbsp;(Aggregate)</h2><hr /><br>
		<label>HSC Marks</label>
		<input type="text" name="hscmk" class="required number"/>&nbsp;/&nbsp;<input type="text" name="hscout" class="required number"/><br />
		
		<label>CET Marks</label>
		<input type="text" name="cetmk" />&nbsp;/&nbsp;<input type="text" name="cetout"/><br />

		<label>AIEEE Marks</label>
		<input type="text" name="aieeemk" />&nbsp;/&nbsp;<input type="text" name="aieeeout"/><br />

		<label>Year of Passing</label>
		<select id="select" name="hscyear">
		<?php
			for($i=2000;$i<=2015;$i++)
			{
				echo "<option>$i</option>";
			}
		?>
		</select><br />
		<?php	
	}
	elseif($type=='Both')
	{
		?>
		<h2>HSC&nbsp;(Aggregate)</h2><hr /><br>
		<label>HSC Marks</label>
		<input type="text" name="hscmk" class="required number"/>&nbsp;/&nbsp;<input type="text" name="hscout" class="required number"/><br />

		<label>CET Marks</label>
		<input type="text" name="cetmk" />&nbsp;/&nbsp;<input type="text" name="cetout"/><br />

		<label>AIEEE Marks</label>
		<input type="text" name="aieeemk" />&nbsp;/&nbsp;<input type="text" name="aieeeout"/><br />

		<label>Year of Passing</label>
		<select id="select" name="hscyear">

		<?php
			for($i=2000;$i<=2015;$i++)
			{
				echo "<option>$i</option>";
			}
		?>
		</select><br />

		<h2>Diploma</h2><hr /><br>
		<label>Branch</label>
		<select name="dipbranch" style="height: 30px;">
			<option>Computer</option>
			<option>IT</option>
			<option>EXTC</option>
			<option>Electrical</option>
			<option>Mechanical</option>
			<option>Electronics</option>
		</select><br /><br />

		<label>Final Year Marks</label>
		<input type="text" name="dipfmk" class="required number"/>&nbsp;/&nbsp;<input type="text" name="dipfout" class="required number"/><br />

		<label>Year of Passing</label>
		<select id="select" name="dipyear">
			<?php
			for($i=2000;$i<=2015;$i++)
			{
				echo "<option>$i</option>";
			}
			?>
		</select><br /><br />
		
		<label>Aggregate Marks</label>
		<input type="text" name="dipaggmk" class="required number" />&nbsp;/&nbsp;<input type="text" name="dipaggout" class="required number" /><br />
		<?php	
	}
}
?>