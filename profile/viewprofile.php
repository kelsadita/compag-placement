<?php	
	include '../includes/header.inc.php';
	include '../includes/connect.inc.php';
	include '../includes/session.inc.php';
	$db = mysqli_connect(DB_HOST, DB_USER, DB_PASSWORD, DB_NAME);
	$query1 = "select * from user where user_id = ".$_SESSION['user_id'];
	$personal_data = mysqli_query($db, $query1);
	$query2 = "select * from marks where user_id = ".$_SESSION['user_id'];
	$marks_data = mysqli_query($db, $query2);

	while( $personal_row = mysqli_fetch_array($personal_data)  )
	{
		//Personal information
		$roll_no                          = $personal_row['roll_no'];
		$branch                           = $personal_row['branch'];
		$fname                            = $personal_row['fname'];
		$mname                            = $personal_row['mname'];
		$lname                            = $personal_row['lname'];
		$gender                           = $personal_row['gender'];
		$contact_no                       = $personal_row['contact_no'];
		$dob                              = $personal_row['dob'];
		$email                            = $personal_row['email'];
		$username                         = $fname +" " + $mname +" "+ $lname;
		$address			  			  = $personal_row['address'];
		$placed_in			        	  = $personal_row['placed_in'];
	}

	while($marks_row = mysqli_fetch_array($marks_data)){
		//SSC data
		$sscmk                            = $marks_row['sscmk'];
		$sscout                           = $marks_row['sscout'];
		$sscyear                          = $marks_row['sscyear'];

		//Hsc data
		$hscmk                            = $marks_row['hscmk'];
		$hscout                           = $marks_row['hscout'];
		$hscyear                          = $marks_row['hscyear'];

		//CET/AIEEE
		$cetmk							  = $marks_row['cetmk'];
		$cetout							  = $marks_row['cetout'];
		$aieeemk						  = $marks_row['aieeemk'];
		$aieeeout						  = $marks_row['aieeeout'];

		//Diploma related inforamtion
		$dipbranch                        = $marks_row['dipbranch'];
		$dipyear                          = $marks_row['dipyear'];
		$dipfmk                           = $marks_row['dipfmk'];
		$dipfout                          = $marks_row['dipfout'];
		$dipaggmk                         = $marks_row['dipaggmk'];
		$dipaggout                        = $marks_row['dipaggout'];

		//semesterwise marks distrubution
		$sem1mk                           = $marks_row['sem1mk'];
		$sem1out                          = $marks_row['sem1out'];
		$sem2mk                           = $marks_row['sem2mk'];
		$sem2out                          = $marks_row['sem2out'];
		$sem3mk                           = $marks_row['sem3mk'];
		$sem3out                          = $marks_row['sem3out'];
		$sem4mk                           = $marks_row['sem4mk'];
		$sem4out                          = $marks_row['sem4out'];
		$sem5mk                           = $marks_row['sem5mk'];
		$sem5out                          = $marks_row['sem5out'];
		$sem6mk                           = $marks_row['sem6mk'];
		$sem6out                          = $marks_row['sem6out'];
		$aggmk                            = $marks_row['aggmk'];
		$aggout                           = $marks_row['aggout'];

		//kt info
		$livekt                           = $marks_row['livekt'];
		$deadkt                           = $marks_row['deadkt'];

		//percentage data
		$sscper                           = $marks_row['sscper'];
		$hscper                           = $marks_row['hscper'];

		$dipfper                          = $marks_row['dipfper'];
		$dipaggper                        = $marks_row['dipaggper'];

		$sem1per                          = $marks_row['sem1per'];
		$sem2per                          = $marks_row['sem2per'];
		$sem3per                          = $marks_row['sem3per'];
		$sem4per                          = $marks_row['sem4per'];
		$sem5per                          = $marks_row['sem5per'];
		$sem6per                          = $marks_row['sem6per'];

		$finalagg                         = $marks_row['finalagg'];

			
	}

?>

<div class="linkcontent" style = "margin-top: 60px; width: 600px;">
	<?php
	if(isset($_SESSION['update']))
	{
		 unset($_SESSION['update']);
		 echo '<p class="notify">Your profile is updated successfully.</p>';
	}
	if(isset($_SESSION['first']))
	{
		 unset($_SESSION['first']);
		 echo '<p class="notify">Your form has been successfully filled.</p>';
	}
	?>
<?php
if($placed_in!="")
{
	if($placed_in!="Not in the process")
	{
		?>
		<h2><font color="red">Congrats!! You got placed in <?php echo $placed_in;?></font></h2>
		<?php
	}
	else
	{
		?>
		<h2><font color="red">Sorry, you have been excluded from placement process!!!</font></h2>
		<?php
	}
}
?>
	<hr noshade style = "border: 2px solid #CCCCCC;" /> 
	<h2>Personal Information<?php if($placed_in=="") {?><a class="orange" style="float:right;font-size:13px;text-decoration: none;" href="edit_profile.php">Edit</a><?php } ?></h2>
	<hr noshade style = "border: 2px solid #CCCCCC;" /> 
	<table cellpadding = "7">
	    <tr><td><b class = "vpelements">First&nbsp;Name</b></td><td><?= $fname;?></td></tr>
	    <tr><td><b class = "vpelements">Middle&nbsp;Name</b></td><td><?= $mname;?></td></tr>
	    <tr><td><b class = "vpelements">Last&nbsp;Name</b></td><td><?= $lname;?></td></tr>
	    <tr><td><b class = "vpelements">Gender</b></td><td><?= $gender;?></td></tr>
	    <tr><td><b class = "vpelements">Roll&nbsp;No.</b></td><td><?= $roll_no;?></td></tr>
	    <tr><td><b class = "vpelements">Branch</b></td><td><?= $branch;?></td></tr>
	    <tr><td><b class = "vpelements">Contact&nbsp;No.</b></td><td><?= $contact_no;?></td></tr>
	    <tr><td><b class = "vpelements">Birth Date</b></td><td><?= $dob;?></td></tr>
	    <tr><td><b class = "vpelements">Email</b></td><td><?= $email;?></td></tr>
	    <tr><td><b class = "vpelements">Address</b></td><td><?= $address;?></td></tr>
	</table>
	<br /><br />	
	<hr noshade style = "border: 2px solid #CCCCCC;" /> 
    	<h2>Mark Details<?php /*if($placed_in=="") {?><a class="orange" style="float:right;font-size:13px;text-decoration: none;" href="edit_form.php">Edit</a><?php } */?></h2>
	<hr noshade style = "border: 2px solid #CCCCCC;" /> 
	<br/><br/>	
	<!--Entering SSC data-->
	<p class = "heading">SSC Information</p>
	<table cellpadding = "7" style = "width: 484px;">
	    <tr><td><b class = "vpelements">SSC Year</b></td><td><?= $sscyear;?></td></tr>
	    <tr>
		<td><b class = "vpelements">SSC Marks</b></td><td><?= $sscmk;?>&nbsp;/&nbsp;<?= $sscout; ?></td>
	    	<td><b class = "vpelements">percentage</b></td><td><?= $sscper; ?></td>
	    </tr>
	</table>

	<?php 
	if($cetmk != 0)
	{
	?>	
	
	<p class = "heading">CET Marks</p>
	<table cellpadding = "7"  style = "width: 484px;">
	    <tr>
			<td><b class = "vpelements">CET Marks</b></td>
			<td><?= $cetmk;?>&nbsp;/&nbsp;<?= $cetout; ?></td>
	    </tr>
	    <tr>
	</table>

	<?php
	}
	?>

	<?php 
	if($aieeemk != 0)
	{
	?>	
	
	<p class = "heading">AIEEE Marks</p>
	<table cellpadding = "7"  style = "width: 484px;">
	    <tr>
			<td><b class = "vpelements">AIEEE Marks</b></td>
			<td><?= $aieeemk;?>&nbsp;/&nbsp;<?= $aieeeout; ?></td>
	    </tr>
	    <tr>
	</table>

	<?php
	}
	?>

	<!--Entering diploma data -->
	<?php if($dipfmk != 0 && $hscmk == 0){?>	
	
	<p class = "heading">Diploma Information</p>
	<table cellpadding = "7"  style = "width: 484px;">
	    <tr><td><b class = "vpelements">Diploma Year</b></td><td><?= $dipyear;?></td></tr>
	    <tr><td><b class = "vpelements">Diploma Branch</b></td><td><?= $dipbranch;?></td></tr>
	    <tr>
		<td><b class = "vpelements">Final Year Marks</b></td><td><?= $dipfmk;?>&nbsp;/&nbsp;<?= $dipfout; ?></td>
	    	<td><b class = "vpelements">percentage</b></td><td><?= $dipfper; ?></td>
	    </tr>
	    <tr>
		<td><b class = "vpelements">Aggregate Marks</b></td><td><?= $dipaggmk;?>&nbsp;/&nbsp;<?= $dipaggout; ?></td>
	    	<td><b class = "vpelements">percentage</b></td><td><?= $dipaggper; ?></td>
	    </tr>
	</table>
	
	
	<?php 
	}

	elseif($hscmk != 0 && $dipfmk == 0)
	{ 

	?>
	<!--Entering HSC data-->
	<p class = "heading">HSC Information</p>
	<table cellpadding = "7"  style = "width: 484px;">
	    <tr><td><b class = "vpelements">HSC Year</b></td><td><?= $hscyear;?></td></tr>
	    <tr>
		<td><b class = "vpelements">HSC Marks</b></td><td><?= $hscmk;?>&nbsp;/&nbsp;<?= $hscout; ?></td>
	    	<td><b class = "vpelements">percentage</b></td><td><?= $hscper; ?></td>
	    </tr>
	</table>
	<?php 
	}

	elseif($hscmk != 0 && $dipfmk != 0) 
	{
		?>
		<p class = "heading">HSC Information</p>
		<table cellpadding = "7"  style = "width: 484px;">
		    <tr><td><b class = "vpelements">HSC Year</b></td><td><?= $hscyear;?></td></tr>
		    <tr>
			<td><b class = "vpelements">HSC Marks</b></td><td><?= $hscmk;?>&nbsp;/&nbsp;<?= $hscout; ?></td>
		    	<td><b class = "vpelements">percentage</b></td><td><?= $hscper; ?></td>
		    </tr>
		</table>

		<p class = "heading">Diploma Information</p>
		<table cellpadding = "7"  style = "width: 484px;">
		    <tr><td><b class = "vpelements">Diploma Year</b></td><td><?= $dipyear;?></td></tr>
		    <tr><td><b class = "vpelements">Diploma Branch</b></td><td><?= $dipbranch;?></td></tr>
		    <tr>
			<td><b class = "vpelements">Final Year Marks</b></td><td><?= $dipfmk;?>&nbsp;/&nbsp;<?= $dipfout; ?></td>
		    	<td><b class = "vpelements">percentage</b></td><td><?= $dipfper; ?></td>
		    </tr>
		    <tr>
			<td><b class = "vpelements">Aggregate Marks</b></td><td><?= $dipaggmk;?>&nbsp;/&nbsp;<?= $dipaggout; ?></td>
		    	<td><b class = "vpelements">percentage</b></td><td><?= $dipaggper; ?></td>
		    </tr>
		</table>
		<?php
	}
	?>

	<p class = "heading">Engineering Degree Information</p>
	<table cellpadding = "7" >
	<?php if($dipfmk == 0){ ?>
	<tr>
	    <td><b class = "vpelements">Semester 1</b></td><td><?= $sem1mk;?>&nbsp;/&nbsp;<?= $sem1out; ?></td>
	    <td><b class = "vpelements">percentage</b></td><td><?= $sem1per; ?></td>
	</tr>
	<tr>
	    <td><b class = "vpelements">Semester 2</b></td><td><?= $sem2mk;?>&nbsp;/&nbsp;<?= $sem2out; ?></td>
	    <td><b class = "vpelements">percentage</b></td><td><?= $sem2per; ?></td>
	</tr>
	<?php } ?>
		
	<tr>
	    <td><b class = "vpelements">Semester 3</b></td><td><?= $sem3mk;?>&nbsp;/&nbsp;<?= $sem3out; ?></td>
	    <td><b class = "vpelements">percentage</b></td><td><?= $sem3per; ?></td>
	</tr>
	<tr>
	    <td><b class = "vpelements">Semester 4</b></td><td><?= $sem4mk;?>&nbsp;/&nbsp;<?= $sem4out; ?></td>
	    <td><b class = "vpelements">percentage</b></td><td><?= $sem4per; ?></td>
	</tr>
	<tr>
	    <td><b class = "vpelements">Semester 5</b></td><td><?= $sem5mk;?>&nbsp;/&nbsp;<?= $sem5out; ?></td>
	    <td><b class = "vpelements">percentage</b></td><td><?= $sem5per; ?></td>
	</tr>
	<tr>
	    <td><b class = "vpelements">Semester 6</b></td><td><?= $sem6mk;?>&nbsp;/&nbsp;<?= $sem6out; ?></td>
	    <td><b class = "vpelements">percentage</b></td><td><?= $sem6per; ?></td>
	</tr>
	<tr>
	    <td><b class = "vpelements">Aggregate </b></td><td><?= $aggmk;?>&nbsp;/&nbsp;<?= $aggout; ?></td>
	    <td><b class = "vpelements">percentage</b></td><td><?= $finalagg; ?></td>
	</tr>
    	</table>

	<?php if($livekt != 0 || $deadkt != 0){ ?>

	<p class = "heading">KT Information</p>
	<table cellpadding = "7">
	    <tr><td><b class = "vpelements">Live KT</b></td><td><?= $livekt;?></td></tr>
	    <tr><td><b class = "vpelements">Dead KT</b></td><td><?= $deadkt;?></td></tr>
	</table>
	<?}?>
</div>
<?php 
	include '../includes/footer.inc.php';
?>