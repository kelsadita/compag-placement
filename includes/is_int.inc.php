<?php
    function int($int)
    {
      // First check if it's a numeric value as either a string or number
        if(is_numeric($int) === TRUE)
        {    
            // It's a number, but it has to be an integer
            if((int)$int == $int)
            {
                return TRUE;   
            // It's a number, but not an integer, so we fail
            }
            else
            {
                return FALSE;
            }
        // Not a number
        }
        else
        {
            return FALSE;
        }
    }
?>