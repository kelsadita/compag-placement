<?php 
ob_start();
session_start();

// Uncomment following in production.
error_reporting(0);

if (get_magic_quotes_gpc()) {
    $process = array(&$_GET, &$_POST, &$_COOKIE, &$_REQUEST);
    while (list($key, $val) = each($process)) {
        foreach ($val as $k => $v) {
            unset($process[$key][$k]);
            if (is_array($v)) {
                $process[$key][stripslashes($k)] = $v;
                $process[] = &$process[$key][stripslashes($k)];
            } else {
                $process[$key][stripslashes($k)] = stripslashes($v);
            }
        }
    }
    unset($process);
}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head profile="http://gmpg.org/xfn/11">

<title>Placement | Compag</title>

<!--Meta Tags-->
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<?php 


	
// Define application constants
  define('PP_UPLOADPATH', 'pimages/');
  define('PP_MAXFILESIZE', 1712000);      // 32 KB
  define('PP_MAXIMGWIDTH', 1800);        // 120 pixels
  define('PP_MAXIMGHEIGHT', 1800);       // 120 pixels
  define('baseurl', 'http://placement.compag.in/');   
?>
	<!--Links for css-->
	<link href='http://fonts.googleapis.com/css?family=Raleway' rel='stylesheet' type='text/css'>
	<link rel="shortcut icon" href="<?php echo baseurl; ?>images/favicon.ico" type="image/x-icon" />
	<link rel="stylesheet" href="<?php echo baseurl; ?>css/style.css" type="text/css" media="screen and (min-width: 481px)" />
	<link rel="stylesheet" href="<?php echo baseurl; ?>css/linkstyle.css" type="text/css" media="screen and (min-width: 481px)" />
	<link rel="stylesheet" href="<?php echo baseurl; ?>css/jqueryui.css" type="text/css" media="screen and (min-width: 481px)" />
	

	<link rel="stylesheet" href="<?php echo baseurl; ?>css/default.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="<?php echo baseurl; ?>css/nivo-slider.css" type="text/css" media="screen" />
	

	<link rel="stylesheet" type="text/css" href="<?php echo baseurl; ?>css/flexigrid.pack.css" />

	<!--Java script source files -->
	<script type="text/javascript" src="<?php echo baseurl; ?>js/jquery-latest.js"></script>
	<script type="text/javascript" src="<?php echo baseurl; ?>js/flexigrid.pack.js"></script>
	<script type="text/javascript" src="<?php echo baseurl; ?>js/jqueryui.js"></script>
 	<script type="text/javascript" src="<?php echo baseurl; ?>js/jquery.nivo.slider.pack.js"></script>
	<script type="text/javascript" src="<?php echo baseurl; ?>js/jquery.colorbox.js"></script>
	<script type="text/javascript" src="<?php echo baseurl; ?>js/jquery.validate.js"></script>
	<script type="text/javascript">
		$(document).ready(function(){
		    	
			//validating form
			$("#form").validate({ 
			    
				rpassword:{
					required: true,
					equalTo: "#password" 
				},    
			});

			//resizing side according to browser.
			var clientWidth = $(document).width(); 
			$("#header, .band").css("width", clientWidth);
			//yo vipul add comment here :)
			$("#main").change( function(){
				var category = $( this ).val();
				if(category !== "Select Option"){
					if(category === "HSC"){
						$("#markform").show();
						$("#degree").show();
					} else { 
						$("#markform").show();
						$("#degree").hide();
					}
				}else { 
					$("#markform").hide();
				}
				$( "#loadContent" ).load("hsc-dip.php?type="+category);	
			});

			
			$("#live").change( function(){
				var num = $( this ).val();
				for(var i = 0; i < num; i ++){
					$( "#livekt" ).add("<input type='text' name='LKT'"+(i+1)+" id='LKT'"+(i+1)+"/><br />");
				}
			});
			$("#dead").change( function(){
		    	var num = $( this ).val();
			$( "#deadkt" ).load("dkt.php?num="+num);	
			});
			//Date picker for entering birthdate
			$("#birthday").datepicker({
				dateFormat: 'yy-mm-dd',
				changeMonth: true,
    				changeYear: true,
    				yearRange: '1985:2000'
			});

			//Nivo slider plugin
			$('#slider').nivoSlider();
	
		});
	</script>
	<style>
		a {
			text-decoration: none;
		}
	</style>
</head>
<body>
<div id="header" >
<div style ="width: 900px; margin: 0 auto;">
<span>
<a href="http://www.compag.in/home">
<image src="<?php echo baseurl; ?>images/compag.png" style="float: left; height : 45px; padding-top: 20px;"/>
</a>
</span>


<span style = "width : 100%">
    	<?php
		if (basename( $_SERVER['PHP_SELF'] ) === "index.php" && $_SERVER['PHP_SELF'] !== '/placement/admin/index.php') {	
	?>
			<div style = "float : right; margin : 25px 10px 0px 0px; ">	
			    <a href ="<?php echo baseurl; ?>login/approve.php"><button  class ="orange">Register here &rarr;&nbsp;</button></a><br /><br />
			</div>
	<?php
		}elseif(isset($_SESSION['user_id'])){  
			$pagename = basename( $_SERVER['PHP_SELF'] );
		
	?>
			<div style = "float : right; margin : 18px 10px 0px 0px; ">
			    <?php 
			    if(!isset($_SESSION['status']))
			    {
				    
				    	?>
					    <a href ="<?php echo baseurl; ?>home.php"><button <?php if($pagename === "form.php" || $pagename === "viewprofile.php") echo 'style="border: 2px solid #CCCCCC;"';?> class ="orange">Home</button></a> 
					    <a href ="<?php echo baseurl; ?>tabs/contact_admin.php"><button <?php if($pagename === "contact_admin.php") echo 'style="border: 2px solid #CCCCCC;"';?> class ="orange">Contact Admin</button></a>
					    <a href ="<?php echo baseurl; ?>login/change_password.php"><button <?php if($pagename === "change_password.php") echo 'style="border: 2px solid #CCCCCC;"';?> class ="orange">Change Password</button></a>
					    <?php 
				}
				if(isset($_SESSION['status']))
			    {
				    if($_SESSION['status'] == "admin")
			    { 
			    	?>
			    	
			    	<a href ="<?php echo baseurl; ?>admin/records.php"><button <?php if($pagename === "records.php") echo 'style="border: 2px solid #CCCCCC;"';?> class ="orange">Records</button></a> 
			    	<a href ="<?php echo baseurl; ?>admin/track.php"><button <?php if($pagename === "track.php") echo 'style="border: 2px solid #CCCCCC;"';?> class ="orange">Track</button></a> 
			    	<a href ="<?php echo baseurl; ?>admin/export.php"><button <?php if($pagename === "export.php") echo 'style="border: 2px solid #CCCCCC;"';?> class ="orange">Export</button></a> 
			    	<a href ="<?php echo baseurl; ?>admin/placements.php?ref=1&company=TCS"><button <?php if($pagename === "placements.php") echo 'style="border: 2px solid #CCCCCC;"';?> class ="orange">Placements</button></a> 
			    	
			    	
			    <?php }} ?>
			    <a href ="<?php echo baseurl; ?>login/logout.php"><button  class ="orange">Logout</button></a><br /><br />
			</div>
		
	<?php
		}
	?>

	</span>

</div>
</div>
<div class ="band">
</div>