<style>

table tr td{
	min-height : 800px;
}

a:hover{
	text-decoration : underline;
}
label.error { 
	width:200px; 
	float: none; 
	color: red; 
	vertical-align:top;
}
form label{
	vertical-align : top;
}
textarea{
	width : 250px;
	height : 150px;
}
</style>
</head>

<div class = "linkcontent" style = "padding-top:  : 30px;">
<table width = "100%" style = "margin-top : 30px;">
	<tr>
	<?php $this->load->view('includes/sidebar');?>
	<td width = "80%">
		<a class = "dbutton" style = "float:right;" href = "<?php echo base_url();?>pings" >Messages</a>
		<h1 style = "color : #3b608a;">Write Message</h1>
		<hr noshade style = "border: 2px solid #CCCCCC;"><br />
		
        <?php
        	
        	if ($this->session->flashdata('notice')) {
				echo '<p class="notify">'.$this->session->flashdata('notice').'</p>';
			}
        ?>            
		<br /><br />
               
                <script>
                    $(document).ready(function(){
                    $("input#autocomplete").autocomplete({
                    
                
    source: [<?php foreach ($usernames as $username){ echo '"'.$username['username'].'",';} ?>]
               
                         });
                    });
                </script>
                
		<form action="<?php echo base_url(); ?>pings/send_msg" method = "post" id="msgform">
            <label for = "to">To : </label>
			<input id ="autocomplete" type = "text" name = "to" class = "required"/><br /><br />
			<label for = "message">Message : </label>
			<textarea name = "message" class = "required"></textarea><br /><br />
			<label>&nbsp;</label>
			<input type = "submit" name = "submit" value = "send" />
		</form>
	</td>
	</tr>
</table>

</div>