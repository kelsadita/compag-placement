<?php 	
	include '../includes/header.inc.php';
	include '../includes/connect.inc.php';
	include '../includes/session.inc.php';
?>
<div class="linkcontent" >
	<?php
	if(!isset($_SESSION['user_id']))
	{
		$base=baseurl;
		header("location: $base");
	}
	if(isset($_SESSION['pc']))
	{
		unset($_SESSION['pc']);
		echo '<p class="notify">Your password has been changed successfully..</p>';
	}
	if(isset($_POST['update']))
	{
		extract($_POST);
		if(!empty($old)&&!empty($new)&&!empty($cnew))
		{
			$user_id=$_SESSION['user_id'];
			$db=mysqli_connect(DB_HOST,DB_USER,DB_PASSWORD,DB_NAME);
			$query="select * from user where user_id=$user_id";
			$data=mysqli_query($db,$query);
			$row=mysqli_fetch_array($data);
			if($row['password']==SHA1($old))
			{
				if($new==$cnew)
				{
					if(SHA1($new)!=$row['password'])
					{
						$query="update user set password=SHA('$new') where user_id=$user_id";
						if(mysqli_query($db,$query))
						{
							$_SESSION['pc']=1;
							header('Location: change_password.php');
						}
						else
						{
							echo 'error';
						}
					}
					else
					{
						echo '<p class="notify">Old and new password can not be same..</p>';
						
					}
				}
				else
				{
					echo '<p class="notify">Enter confirm password same..</p>';
				}
			}				
			else
			{
				echo '<p class="notify">Old Password do not matched</p>';
			}		
		}
		
		else
		{
			echo '<p class="notify">Enter all information</p>';
		}
	}
	?>
	<h2>Change Password</h2>
	<hr noshade style = "border: 2px solid #CCCCCC;" /><br/><br/>
	<form class="cmxform" id="form" action="<?php echo $_SERVER['PHP_SELF'];?>" method="post">
		<label for="old">Old Password</label>
		<input type="password" name="old"/><br /><br />
		<label for="new">New Password</label>
		<input type="password" name="new" /><br /><br />
		<label for="cnew">Confirm New Password</label>
		<input type="password" name="cnew" /><br /><br />
		<label>&nbsp;</label>
		<input type="submit" value="update" name="update" class="button"/>
	</form>
	</div>
<?php include '../includes/footer.inc.php';?>