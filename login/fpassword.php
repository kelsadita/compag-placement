<?php
	include '../includes/header.inc.php';
	include '../includes/connect.inc.php'; 
?>
<style type="text/css">
label.error { width:200px; float: none; color: red; vertical-align:top;}
</style>

<div class = "linkcontent" style = "padding-top:50px;">
	
	<?php
		
		$db = mysqli_connect(DB_HOST, DB_USER, DB_PASSWORD, DB_NAME);
		
		if(isset($_POST['submit']))
		{
			$to = mysqli_real_escape_string($db,trim($_POST['email']));
			
			$search_query = "select * from user where email = '$to'";
			$search_data =  mysqli_query($db,$search_query);
		
			if(mysqli_num_rows($search_data) != 1)
			{
				echo '<p class = "notify">please enter the email id you provided to us!.</p>';
			}
			else
			{
				$confir_id = getrandom();
				$query = "insert into rpassword values('$to','$confir_id')";
				if(mysqli_query($db,$query))
				{
					$from = "From:noreply@compag.in";
					$subject = "Reset Password";
					$message = "click this link and reset the password of your FCRIT placement portal account http://placement.compag.in/login/resetpassword.php?confir_id=$confir_id";
					$headers = $from;
					mail($to,$subject,$message,$headers);
					
					echo '<p class = "notify">now check your inbox for reset password link.</p>';
				}
				else
				{
					echo '<p class = "notify">your request has been already submitted.</p>';
				}
			}
		}
		
		function getrandom()
		{
			$length = 10;
			$characters = '123456789abcdefghijklmnopqrstuvwxyz';
			$string = '';    
			for ($p = 0; $p < $length; $p++) 
			{
				$string .= $characters[mt_rand(0, strlen($characters)-1)];
			}
			return $string;
		}
	
	?>
	
	<div class = "sform" style ="width:600px; margin:0 auto;">
		<hr noshade style = "border: 2px solid #CCCCCC;"/>
		<h2>forgot your password ? <a style="float:right; text-decoration: none; font-size: 13px;" class="orange" href="<?= baseurl;?>">Back</a></h2>
		<hr noshade style = "border: 2px solid #CCCCCC;"/>
		<p>To reset your password,type the full email address you use to sign in to your acount.</p>
		<p>you will get password reset link at this email</p>
		<form action = "<?php echo $_SERVER['PHP_SELF'];?>" method = "post" id = "form">
			<label for = "email">email : </label>
			<input type = "text" id = "email" name = "email" class = "required email"/><br />
			<label>&nbsp;</label>
			<input type = "submit" id = "submit" name = "submit" value = "submit" class = "button"/>		
		</form>
	</div>
</div>
<?php include '../includes/footer.inc.php';?>