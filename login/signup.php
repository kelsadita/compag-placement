<?php include '../includes/header.inc.php'; ?>
<div class="linkcontent">
	
<?php
	if( isset( $_POST[ 'submit' ] ) )
	{ 	
		include '../includes/connect.inc.php';
		extract($_POST);
		
		$db = mysqli_connect(DB_HOST, DB_USER, DB_PASSWORD, DB_NAME);

		$confir_id    = mysqli_real_escape_string($db, trim($confir_id));
		$mail_roll_no = mysqli_real_escape_string($db, trim($roll_no));
		$email		  = mysqli_real_escape_string($db, trim($email));
		$fname        = mysqli_real_escape_string($db, trim($fname));
		$mname        = mysqli_real_escape_string($db, trim($mname));
		$lname        = mysqli_real_escape_string($db, trim($lname));
		$gender       = mysqli_real_escape_string($db, trim($gender));
		$password     = mysqli_real_escape_string($db, trim($password));
		$rpassword    = mysqli_real_escape_string($db, trim($rpassword));
		$contact      = mysqli_real_escape_string($db, trim($contact));
		$dob          = mysqli_real_escape_string($db, trim($dob));
		$address      = mysqli_real_escape_string($db, trim($address));

		if (!empty($fname) && !empty($contact) && !empty($dob) && !empty($address) && !empty($password) && !empty($rpassword)) {
		
		if(strlen($contact)>10||strlen($contact)<10)
		{
			
	?>
			<center>
    				<br /><br /><br />
    				<h2>Contact Number's length should be of 10 digits only</h2>
				<br />
				
    				<h2>
				    <a href = "<?php echo baseurl;?>login/register.php?confir_id=<?php echo $confir_id;?>&roll_no=<?php echo $mail_roll_no; ?>"><img src = "<?php echo baseurl; ?>images/goback.jpg" width="150" height="151" alt="go back image"/><br/ ><h2>Go Back</h2></a>
				</h2>
			</center>
	<?php
			exit;
		}
			
		if($password === $rpassword)
		{
			$branch_code = floor($mail_roll_no / 10000);
			switch( $branch_code )
			{ 		
				case 10: 
					$branch = "Computer";
					break;
				case 20: 
					$branch = "Mechanical";
					break;
				case 30: 
					$branch = "EXTC";
					break;
				case 40: 
					$branch = "Electrical";
					break;
				case 50: 
					$branch = "IT";
					break;
				default: 
					$branch = "no such branch :P";
			}
				
				
			//$query = "update user set branch = '$branch', fname = '$fname', mname = '$mname', lname = '$lname', gender = '$gender', password =  SHA('$password'), contact_no =  $contact, dob = '$dob', address = '$address' where roll_no = $mail_roll_no and hash = '$confir_id'";
			$query = "insert into user (email, roll_no, branch, fname, mname, lname, gender, password, contact_no, dob, address) values ('$email', $roll_no, '$branch', '$fname', '$mname', '$lname', '$gender', SHA('$password'), $contact, '$dob', '$address')";
				if(mysqli_query($db, $query))
				{
					echo "<center>
					    	<br /><br /><br />
					    	<h2>You have been successfully registered!<h2>
						    <br />
						    <a href=".baseurl." class=a><img src=".baseurl."images/login.jpg alt=login ><br />Login Here</a>
						</center>";
				
				} 

				else 
				{ 
					echo $query;
					echo "<center><br /><br /><br /><h1>Database problem!</h1></center>";
				}
			}
			else 
			{ 
?>
				
			<center>
    				<br /><br /><br />
    				<h2>Passwords are not matching!</h2>
				<br />
				
    				<h2>
				    <a href = "<?php echo baseurl;?>login/register.php?confir_id=<?php echo $confir_id;?>&roll_no=<?php echo $mail_roll_no; ?>"><img src = "<?php echo baseurl; ?>images/goback.jpg" width="150" height="151" alt="go back image"/><br/ ><h2>Go Back</h2></a>
				</h2>
			</center>
<?php
			
			}		
		} 
	}
//Chnages made by Vipul Honrao 12/07.2014 1:59:00 PM
	/*****Start*****/
	else
	{
		header("Location: ".baseurl);
	}
	/*****End*****/
?>
</div>

<?php
	include '../includes/footer.inc.php';
?>