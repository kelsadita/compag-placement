<?php
	session_start();	
	if(isset($_SESSION['user_id']))
	{
		$_SESSION = array(); //initialising session to the null array.
		if(isset($_COOKIE[session_name()]))
		{
			setcookie(session_name(),'',time()-3600);
		}	
		session_destroy();
	}
	setcookie('username','',time()-3600);
	setcookie('user_id','',time()-3600);
	setcookie('roll_no','',time()-3600);
	setcookie('branch','',time()-3600);
	setcookie('email','',time()-3600);

	header("Location: http://placement.compag.in");
?>