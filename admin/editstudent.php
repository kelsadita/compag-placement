<?php
  include '../includes/header.inc.php';
  include '../includes/connect.inc.php';
  include '../includes/session.inc.php';
?>
  
<div class="linkcontent" style = "margin-top:40px; width: 800px;">
  
<?php 
  $db = mysqli_connect(DB_HOST, DB_USER, DB_PASSWORD, DB_NAME);
  


  if(isset($_GET['roll_no']))
  {
    $roll_no = $_GET['roll_no'];

    $query = "select * from student where roll_no=".$roll_no;
    $data1 = mysqli_query($db, $query);
    $data2 = mysqli_query($db, $query);
    $row = mysqli_fetch_array($data1);

    if ($data2->num_rows === 0)
    {
      echo "<center><h1>Student with this roll number does not exist!</center></h1>";
      exit(0);
    }

  }
  else
  {
    echo "<center><h1>Student with this roll number does not exist!</center></h1>";
    exit(0);
  }


  if(isset($_POST['submit']))
  {

    $roll_no = $_POST['rollnumber'];
    $email = $_POST['email'];
    $old_rollnumber = $_POST['old_rollnumber'];

    if (isRollNumberChanged($roll_no, $old_rollnumber)) 
    {
      $query = "select * from student where roll_no=".$roll_no;
      $num_rows = mysqli_query($db, $query)->num_rows;
      if ($num_rows === 1)
      {
        echo "<p class='notify'>Student with roll number ".$roll_no." already exist!</p>"; 
      }
      else
      {
        updateStudent($old_rollnumber, $roll_no, $email, $db);
      }
    }
    else
    {
      updateStudent($old_rollnumber, $roll_no, $email, $db);
    }
  }

  function isRollNumberChanged($roll_no, $old_rollnumber)
  {
    return $roll_no !== $old_rollnumber;
  }

  function updateStudent($old_rollnumber, $roll_no, $email, $db)
  {
    $update_student_query = "update student set roll_no=".$roll_no.", email='".$email."' where roll_no=".$old_rollnumber;
    $update_user_query = "update user set roll_no=".$roll_no.", email='".$email."' where roll_no=".$old_rollnumber;
    mysqli_query($db, $update_student_query);
    mysqli_query($db, $update_user_query);
    $_SESSION['update_msg'] = "<p class='notify'>Student data successfully updated!</p>";
    header("Location: ".baseurl."admin/track.php");
  }

?>

<h1>Edit strudent information</h1>
<hr noshade style = "border: 2px solid #CCCCCC;" /><br />

<form method = "post" id = "form">
  <label for = "fullame">Roll Number : </label>
  <input class = "required number" type = "text" name = "rollnumber" id = "rollnumber" value="<?php echo $row['roll_no']?>" /><br />
  <input type="hidden" name="old_rollnumber" value="<?php echo $row['roll_no']?>" />
  
  <label for = "email">Email : </label>
  <input class = "required email" type = "text" name = "email" id = "email" value="<?php echo $row['email']?>" /><br />
  
  <label>&nbsp;</label>
  <input class = "button" type = "submit" value = "confirm" name = "submit" id = "submit" />
  &nbsp;&nbsp;
  <a href='<?=baseurl?>admin/track.php'>Cancel</a>
</form>
  
</div>

<?php
  include '../includes/footer.inc.php';
?>