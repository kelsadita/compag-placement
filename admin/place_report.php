<?php
	include '../includes/header.inc.php';
	include '../includes/connect.inc.php';
	include '../includes/session.inc.php';

?>

<style>
	
	
	#excel_gen {
	
		border: 1px solid #CCCCCC;
		margin: 0 auto;
		height: 220px;
		width: 750px;
	}
</style>

<div class="linkcontent" style = "margin-top:40px; width: 800px;">
	
<?php

//getting the information about the sheets

	$filename = "overall_placement";
?>
	
	<hr noshade style = "border: 2px solid #CCCCCC;" /><br />

<?php

/** Error reporting */
error_reporting(E_ALL);


/** PHPExcel */
include 'PHPExcel.php';

/** PHPExcel_Writer_Excel2007 */
include 'PHPExcel/Writer/Excel2007.php';


$objPHPExcel = new PHPExcel();


$objPHPExcel->getProperties()->setCreator("Kalpesh Adhatrao");
$objPHPExcel->getProperties()->setLastModifiedBy("Kalpesh Adhatrao");
$objPHPExcel->getProperties()->setTitle("Office 2007 XLSX Test Document");
$objPHPExcel->getProperties()->setSubject("Office 2007 XLSX Test Document");
$objPHPExcel->getProperties()->setDescription("Student data document for Office 2007 XLSX, generated using PHP classes.");



$objPHPExcel->getActiveSheet()->SetCellValue('A1', 'Sr No.');
$objPHPExcel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

$objPHPExcel->getActiveSheet()->SetCellValue('B1', 'COMPANY NAME');
$objPHPExcel->getActiveSheet()->getStyle('B1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

$objPHPExcel->getActiveSheet()->SetCellValue('C1', 'CUT OFF');
$objPHPExcel->getActiveSheet()->getStyle('C1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

$objPHPExcel->getActiveSheet()->SetCellValue('D1', 'COMPUTER');
$objPHPExcel->getActiveSheet()->getStyle('D1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

$objPHPExcel->getActiveSheet()->SetCellValue('E1', 'IT');
$objPHPExcel->getActiveSheet()->getStyle('E1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

$objPHPExcel->getActiveSheet()->SetCellValue('F1', 'MECH');
$objPHPExcel->getActiveSheet()->getStyle('F1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

$objPHPExcel->getActiveSheet()->SetCellValue('G1', 'ELECT');
$objPHPExcel->getActiveSheet()->getStyle('G1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

$objPHPExcel->getActiveSheet()->SetCellValue('H1', 'EXTC');
$objPHPExcel->getActiveSheet()->getStyle('H1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

$objPHPExcel->getActiveSheet()->SetCellValue('I1', 'TOTAL');
$objPHPExcel->getActiveSheet()->getStyle('I1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

$objPHPExcel->getActiveSheet()->SetCellValue('J1', 'SALARY(LPA)');
$objPHPExcel->getActiveSheet()->getStyle('J1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(10);
$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(13);
$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(25);
$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(15);
$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(15);
$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(15);
$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(15);
$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(15);
$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(15);
$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(20);



$default_border = array(
    'style' => PHPExcel_Style_Border::BORDER_THIN,
    'color' => array('rgb'=>'1006A3')
);

$style_header = array(
   'borders' => array(
       	'bottom' => $default_border,
        'left' => $default_border,
        'top' => $default_border,
        'right' => $default_border, 
        
    ),
    
    'fill' => array(
        'type' => PHPExcel_Style_Fill::FILL_SOLID,
        'color' => array('rgb'=>'E1E0F7'),
    ),
    'font' => array(
        'bold' => true,
    )
);
 
//applying styles to the cells
$objPHPExcel->getActiveSheet()->getStyle('A1')->applyFromArray( $style_header );
$objPHPExcel->getActiveSheet()->getStyle('B1')->applyFromArray( $style_header );
$objPHPExcel->getActiveSheet()->getStyle('C1')->applyFromArray( $style_header );
$objPHPExcel->getActiveSheet()->getStyle('D1')->applyFromArray( $style_header );
$objPHPExcel->getActiveSheet()->getStyle('E1')->applyFromArray( $style_header );
$objPHPExcel->getActiveSheet()->getStyle('F1')->applyFromArray( $style_header );
$objPHPExcel->getActiveSheet()->getStyle('G1')->applyFromArray( $style_header );
$objPHPExcel->getActiveSheet()->getStyle('H1')->applyFromArray( $style_header );
$objPHPExcel->getActiveSheet()->getStyle('I1')->applyFromArray( $style_header );
$objPHPExcel->getActiveSheet()->getStyle('J1')->applyFromArray( $style_header );


//Getting marks and personal details of the students and adding it to excel

$db = mysqli_connect(DB_HOST, DB_USER, DB_PASSWORD, DB_NAME);

$company_query = "select * from company";
$company_data = mysqli_query($db, $company_query);

?>

<div id= "excel_gen">

	<center>
	<h2>[ Download the overall statistics ]</h2>
	<a href='http://placement.compag.in/admin/download.php?file=<?= $filename; ?>.xlsx'><img src = "http://placement.compag.in/images/Downloads.png" /></a>
	</center>
</div>
<br />
<hr noshade style = "border: 2px solid #CCCCCC;" />



<?php
$count = 2;
while($row = mysqli_fetch_array($company_data)){
	
	
	$objPHPExcel->getActiveSheet()->SetCellValue('A'.$count, $count - 1);
	$objPHPExcel->getActiveSheet()->SetCellValue('B'.$count, $row['name']);
	$objPHPExcel->getActiveSheet()->SetCellValue('C'.$count, $row['cut_off']);
	
	$comp_query = "select * from user where placed_in = '".$row['name']."' and roll_no like '1%'";
	$comp_data = mysqli_query($db, $comp_query);
	$objPHPExcel->getActiveSheet()->SetCellValue('D'.$count, mysqli_num_rows($comp_data));
	
	$it_query = "select * from user where placed_in = '".$row['name']."' and roll_no like '5%'";
	$it_data = mysqli_query($db, $it_query);
	$objPHPExcel->getActiveSheet()->SetCellValue('E'.$count, mysqli_num_rows($it_data));

	$mech_query = "select * from user where placed_in = '".$row['name']."' and roll_no like '2%'";
	$mech_data = mysqli_query($db, $mech_query);
	$objPHPExcel->getActiveSheet()->SetCellValue('F'.$count, mysqli_num_rows($mech_data));
	
	$elect_query = "select * from user where placed_in = '".$row['name']."' and roll_no like '4%'";
	$elect_data = mysqli_query($db, $elect_query);
	$objPHPExcel->getActiveSheet()->SetCellValue('G'.$count, mysqli_num_rows($elect_data));
	
	$extc_query = "select * from user where placed_in = '".$row['name']."' and roll_no like '3%'";
	$extc_data = mysqli_query($db, $extc_query);
	$objPHPExcel->getActiveSheet()->SetCellValue('H'.$count, mysqli_num_rows($extc_data));
	
	$total = mysqli_num_rows($comp_data) + mysqli_num_rows($it_data) + mysqli_num_rows($elect_data) + mysqli_num_rows($extc_data) + mysqli_num_rows($mech_data);
	
	$objPHPExcel->getActiveSheet()->SetCellValue('I'.$count, $total);
	
	$objPHPExcel->getActiveSheet()->SetCellValue('J'.$count, $row['LPA']);

	
	$count ++;
}



$objPHPExcel->getActiveSheet()->setTitle('Student Data');

	
// Save Excel 2007 file

$objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
$objWriter->save(str_replace('.php', '.xlsx', '/home/compa3h9/public_html/placement/admin/files/'.$filename.'.xlsx'));


?>


</div>

<?php
	
	include '../includes/footer.inc.php';
?>