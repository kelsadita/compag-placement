<?php
	include '../includes/header.inc.php';
	include '../includes/connect.inc.php';
	include '../includes/session.inc.php';
	include '../includes/admincheck.inc.php';

?>

<style>
	#chart_div {
	
		border: 1px solid #CCCCCC;
		margin: 0 auto;
	}
	
	#excel_gen {
	
		border: 1px solid #CCCCCC;
		margin: 0 auto;
		height: 60px;
		width: 750px;
	}
</style>

<div class="linkcontent" style = "margin-top:40px; width: 800px;">
	
<?php

//getting the information about the sheets


	if(isset($_GET['ref'])){
		if ($_GET['ref'] >= 1 && $_GET['ref'] <= 5){
			$ref = $_GET['ref'];
		}else{
			$ref = 1;
		}
	}else{
		$ref = 1;
	}
	
	switch($ref){
	
		case 1:
			$filename = "Computer";
			break;
		case 2:
			$filename = "Mechanical";
			break;
		case 3:
			$filename = "EXTC";
			break;
		case 4:
			$filename = "Electrical";
			break;
		case 5:
			$filename = "IT";
			break;	
	}
	
	if(isset($_GET['range'])){
		
		$range = $_GET['range'];
		
		switch($range){
		
			case 55:
				$filename .= "_above55";
				break;
			
			case 57:
				$filename .= "_above57";
				break;
			
			case 58:
				$filename .= "_above58";
				break;
			
			case 60:
				$filename .= "_above60";
				break;
			
			case "l55":
				$filename .= "_lessthan55";
				break;
			
			default:
				$filename .= "_above55";
				break;
		}
	}else{
	
		$filename .= "_above55";
		$range = 55;
	}
?>
 	<p class = "notify">Records of the students who have not placed.</p>
	<hr noshade style = "border: 2px solid #CCCCCC;" /><br />
		<a <?php if($ref == 1) echo 'style="text-decoration:underline;"';?> class="orange" style="font-size:13px;text-decoration: none;" href="export.php?ref=1">Computer</a>&nbsp;&nbsp;
		<a <?php if($ref == 2) echo 'style="text-decoration:underline;"';?> class="orange" style="font-size:13px;text-decoration: none;" href="export.php?ref=2">Mechanical</a>&nbsp;&nbsp;
		<a <?php if($ref == 3) echo 'style="text-decoration:underline;"';?> class="orange" style="font-size:13px;text-decoration: none;" href="export.php?ref=3">EXTC</a>&nbsp;&nbsp;
		<a <?php if($ref == 4) echo 'style="text-decoration:underline;"';?> class="orange" style="font-size:13px;text-decoration: none;" href="export.php?ref=4">Electrical</a>&nbsp;&nbsp;
		<a <?php if($ref == 5) echo 'style="text-decoration:underline;"';?> class="orange" style="font-size:13px;text-decoration: none;" href="export.php?ref=5">IT</a>
	<br /><br />
	<hr noshade style = "border: 2px solid #CCCCCC;" /><br />

<?php

/** Error reporting */
error_reporting(E_ALL);


/** PHPExcel */
include 'PHPExcel.php';

/** PHPExcel_Writer_Excel2007 */
include 'PHPExcel/Writer/Excel2007.php';


$objPHPExcel = new PHPExcel();


$objPHPExcel->getProperties()->setCreator("Kalpesh Adhatrao");
$objPHPExcel->getProperties()->setLastModifiedBy("Kalpesh Adhatrao");
$objPHPExcel->getProperties()->setTitle("Office 2007 XLSX Test Document");
$objPHPExcel->getProperties()->setSubject("Office 2007 XLSX Test Document");
$objPHPExcel->getProperties()->setDescription("Student data document for Office 2007 XLSX, generated using PHP classes.");



$objPHPExcel->getActiveSheet()->SetCellValue('A1', 'Personal Data');
$objPHPExcel->getActiveSheet()->SetCellValue('A2', 'Roll no.');
$objPHPExcel->getActiveSheet()->SetCellValue('B2', 'Name');
$objPHPExcel->getActiveSheet()->SetCellValue('C2', 'Contact No.');
$objPHPExcel->getActiveSheet()->SetCellValue('D2', 'DOB');
$objPHPExcel->getActiveSheet()->SetCellValue('E2', 'First Name');
$objPHPExcel->getActiveSheet()->SetCellValue('F2', 'Middle Name');
$objPHPExcel->getActiveSheet()->SetCellValue('G2', 'Last Name');
$objPHPExcel->getActiveSheet()->SetCellValue('H2', 'Email ID');
$objPHPExcel->getActiveSheet()->SetCellValue('I2', 'Residence Area');
$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A1:I1');
$objPHPExcel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(13);
$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(25);
$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(18);
$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(13);
$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(13);
$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(13);
$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(28);
$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(40);

$objPHPExcel->setActiveSheetIndex(0)->mergeCells('J1:M1');
$objPHPExcel->getActiveSheet()->SetCellValue('J2', 'MO');
$objPHPExcel->getActiveSheet()->SetCellValue('K2', 'MM');
$objPHPExcel->getActiveSheet()->SetCellValue('L2', '%');
$objPHPExcel->getActiveSheet()->SetCellValue('M2', 'Year');
$objPHPExcel->getActiveSheet()->SetCellValue('J1', 'SSC');
$objPHPExcel->getActiveSheet()->getStyle('J1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

$objPHPExcel->setActiveSheetIndex(0)->mergeCells('N1:Q1');
$objPHPExcel->getActiveSheet()->SetCellValue('N2', 'MO');
$objPHPExcel->getActiveSheet()->SetCellValue('O2', 'MM');
$objPHPExcel->getActiveSheet()->SetCellValue('P2', '%');
$objPHPExcel->getActiveSheet()->SetCellValue('Q2', 'Year');
$objPHPExcel->getActiveSheet()->SetCellValue('N1', 'HSC');
$objPHPExcel->getActiveSheet()->getStyle('N1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

$objPHPExcel->setActiveSheetIndex(0)->mergeCells('R1:S1');
$objPHPExcel->getActiveSheet()->SetCellValue('R1', 'CET');
$objPHPExcel->getActiveSheet()->SetCellValue('R2', 'MO');
$objPHPExcel->getActiveSheet()->SetCellValue('S2', 'MM');
$objPHPExcel->getActiveSheet()->getStyle('R1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$objPHPExcel->getActiveSheet()->getColumnDimension('R')->setWidth(16);
$objPHPExcel->getActiveSheet()->getColumnDimension('S')->setWidth(16);

$objPHPExcel->setActiveSheetIndex(0)->mergeCells('T1:U1');
$objPHPExcel->getActiveSheet()->SetCellValue('T1', 'AIEEE');
$objPHPExcel->getActiveSheet()->SetCellValue('T2', 'MO');
$objPHPExcel->getActiveSheet()->SetCellValue('U2', 'MM');
$objPHPExcel->getActiveSheet()->getStyle('T1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$objPHPExcel->getActiveSheet()->getColumnDimension('T')->setWidth(16);
$objPHPExcel->getActiveSheet()->getColumnDimension('U')->setWidth(16);


$objPHPExcel->setActiveSheetIndex(0)->mergeCells('V1:W1');
$objPHPExcel->getActiveSheet()->SetCellValue('V1', 'Diploma');
$objPHPExcel->getActiveSheet()->SetCellValue('V2', 'Diploma Stream');
$objPHPExcel->getActiveSheet()->SetCellValue('W2', 'Year Of Passing');
$objPHPExcel->getActiveSheet()->getStyle('V1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$objPHPExcel->getActiveSheet()->getColumnDimension('V')->setWidth(16);
$objPHPExcel->getActiveSheet()->getColumnDimension('W')->setWidth(16);

$objPHPExcel->setActiveSheetIndex(0)->mergeCells('X1:AA1');
$objPHPExcel->getActiveSheet()->SetCellValue('X1', 'Diploma (Final Year)');
$objPHPExcel->getActiveSheet()->SetCellValue('X2', 'MO');
$objPHPExcel->getActiveSheet()->SetCellValue('Y2', 'MM');
$objPHPExcel->getActiveSheet()->SetCellValue('Z2', '%');
$objPHPExcel->getActiveSheet()->SetCellValue('AA2', 'Total Aggr');
$objPHPExcel->getActiveSheet()->getStyle('X1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

$objPHPExcel->setActiveSheetIndex(0)->mergeCells('AB1:AD1');
$objPHPExcel->getActiveSheet()->SetCellValue('AB1', 'Sem 1');
$objPHPExcel->getActiveSheet()->SetCellValue('AB2', 'MO');
$objPHPExcel->getActiveSheet()->SetCellValue('AC2', 'MM');
$objPHPExcel->getActiveSheet()->SetCellValue('AD2', '%');
$objPHPExcel->getActiveSheet()->getStyle('AB1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);


$objPHPExcel->setActiveSheetIndex(0)->mergeCells('AE1:AG1');
$objPHPExcel->getActiveSheet()->SetCellValue('AE1', 'Sem 2');
$objPHPExcel->getActiveSheet()->SetCellValue('AE2', 'MO');
$objPHPExcel->getActiveSheet()->SetCellValue('AF2', 'MM');
$objPHPExcel->getActiveSheet()->SetCellValue('AG2', '%');
$objPHPExcel->getActiveSheet()->getStyle('AE1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);


$objPHPExcel->setActiveSheetIndex(0)->mergeCells('AH1:AJ1');
$objPHPExcel->getActiveSheet()->SetCellValue('AH1', 'Sem 3');
$objPHPExcel->getActiveSheet()->SetCellValue('AH2', 'MO');
$objPHPExcel->getActiveSheet()->SetCellValue('AI2', 'MM');
$objPHPExcel->getActiveSheet()->SetCellValue('AJ2', '%');
$objPHPExcel->getActiveSheet()->getStyle('AH1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

$objPHPExcel->setActiveSheetIndex(0)->mergeCells('AK1:AM1');
$objPHPExcel->getActiveSheet()->SetCellValue('AK1', 'Sem 4');
$objPHPExcel->getActiveSheet()->SetCellValue('AK2', 'MO');
$objPHPExcel->getActiveSheet()->SetCellValue('AL2', 'MM');
$objPHPExcel->getActiveSheet()->SetCellValue('AM2', '%');
$objPHPExcel->getActiveSheet()->getStyle('AK1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

$objPHPExcel->setActiveSheetIndex(0)->mergeCells('AN1:AP1');
$objPHPExcel->getActiveSheet()->SetCellValue('AN1', 'Sem 5');
$objPHPExcel->getActiveSheet()->SetCellValue('AN2', 'MO');
$objPHPExcel->getActiveSheet()->SetCellValue('AO2', 'MM');
$objPHPExcel->getActiveSheet()->SetCellValue('AP2', '%');
$objPHPExcel->getActiveSheet()->getStyle('AN1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

$objPHPExcel->setActiveSheetIndex(0)->mergeCells('AQ1:AS1');
$objPHPExcel->getActiveSheet()->SetCellValue('AQ1', 'Sem 6');
$objPHPExcel->getActiveSheet()->SetCellValue('AQ2', 'MO');
$objPHPExcel->getActiveSheet()->SetCellValue('AR2', 'MM');
$objPHPExcel->getActiveSheet()->SetCellValue('AS2', '%');
$objPHPExcel->getActiveSheet()->getStyle('AQ1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

$objPHPExcel->setActiveSheetIndex(0)->mergeCells('AT1:AV1');
$objPHPExcel->getActiveSheet()->SetCellValue('AT1', 'Aggr. (BE)');
$objPHPExcel->getActiveSheet()->SetCellValue('AT2', 'MO');
$objPHPExcel->getActiveSheet()->SetCellValue('AU2', 'MM');
$objPHPExcel->getActiveSheet()->SetCellValue('AV2', '%');
$objPHPExcel->getActiveSheet()->getStyle('AT1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

$objPHPExcel->setActiveSheetIndex(0)->mergeCells('AW1:AX1');
$objPHPExcel->getActiveSheet()->SetCellValue('AW1', 'ATKT');
$objPHPExcel->getActiveSheet()->SetCellValue('AW2', 'LIVE');
$objPHPExcel->getActiveSheet()->SetCellValue('AX2', 'DEAD');
$objPHPExcel->getActiveSheet()->getStyle('AW1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

$default_border = array(
    'style' => PHPExcel_Style_Border::BORDER_THIN,
    'color' => array('rgb'=>'1006A3')
);

$style_header = array(
   'borders' => array(
       	'bottom' => $default_border,
        'left' => $default_border,
        'top' => $default_border,
        'right' => $default_border, 
        
    ),
    
    'fill' => array(
        'type' => PHPExcel_Style_Fill::FILL_SOLID,
        'color' => array('rgb'=>'E1E0F7'),
    ),
    'font' => array(
        'bold' => true,
    )
);
 
//applying styles to the cells
$objPHPExcel->getActiveSheet()->getStyle('A1:I1')->applyFromArray( $style_header );
$objPHPExcel->getActiveSheet()->getStyle('J1:M1')->applyFromArray( $style_header );
$objPHPExcel->getActiveSheet()->getStyle('N1:Q1')->applyFromArray( $style_header );
$objPHPExcel->getActiveSheet()->getStyle('R1:S1')->applyFromArray( $style_header );
$objPHPExcel->getActiveSheet()->getStyle('T1:U1')->applyFromArray( $style_header );
$objPHPExcel->getActiveSheet()->getStyle('V1:W1')->applyFromArray( $style_header );
$objPHPExcel->getActiveSheet()->getStyle('X1:AA1')->applyFromArray( $style_header );
$objPHPExcel->getActiveSheet()->getStyle('AB1:AD1')->applyFromArray( $style_header );
$objPHPExcel->getActiveSheet()->getStyle('AE1:AG1')->applyFromArray( $style_header );
$objPHPExcel->getActiveSheet()->getStyle('AH1:AJ1')->applyFromArray( $style_header );
$objPHPExcel->getActiveSheet()->getStyle('AK1:AM1')->applyFromArray( $style_header );
$objPHPExcel->getActiveSheet()->getStyle('AN1:AP1')->applyFromArray( $style_header );
$objPHPExcel->getActiveSheet()->getStyle('AQ1:AS1')->applyFromArray( $style_header );
$objPHPExcel->getActiveSheet()->getStyle('AT1:AV1')->applyFromArray( $style_header );
$objPHPExcel->getActiveSheet()->getStyle('AW1:AX1')->applyFromArray( $style_header );


$objPHPExcel->getActiveSheet()->getStyle('A2:I2')->applyFromArray( $style_header );
$objPHPExcel->getActiveSheet()->getStyle('J2:M2')->applyFromArray( $style_header );
$objPHPExcel->getActiveSheet()->getStyle('N2:Q2')->applyFromArray( $style_header );
$objPHPExcel->getActiveSheet()->getStyle('R2:S2')->applyFromArray( $style_header );
$objPHPExcel->getActiveSheet()->getStyle('T2:U2')->applyFromArray( $style_header );
$objPHPExcel->getActiveSheet()->getStyle('V2:W2')->applyFromArray( $style_header );
$objPHPExcel->getActiveSheet()->getStyle('X2:AA2')->applyFromArray( $style_header );
$objPHPExcel->getActiveSheet()->getStyle('AB2:AD2')->applyFromArray( $style_header );
$objPHPExcel->getActiveSheet()->getStyle('AE2:AG2')->applyFromArray( $style_header );
$objPHPExcel->getActiveSheet()->getStyle('AH2:AJ2')->applyFromArray( $style_header );
$objPHPExcel->getActiveSheet()->getStyle('AK2:AM2')->applyFromArray( $style_header );
$objPHPExcel->getActiveSheet()->getStyle('AN2:AP2')->applyFromArray( $style_header );
$objPHPExcel->getActiveSheet()->getStyle('AQ2:AS2')->applyFromArray( $style_header );
$objPHPExcel->getActiveSheet()->getStyle('AT2:AV2')->applyFromArray( $style_header );
$objPHPExcel->getActiveSheet()->getStyle('AW2:AX2')->applyFromArray( $style_header );



//Getting marks and personal details of the students and adding it to excel
$db = mysqli_connect(DB_HOST, DB_USER, DB_PASSWORD, DB_NAME);
$data_query = "select * from user natural join marks where roll_no like '".$ref."%'";
if(isset($_GET['range'])){
	$range = $_GET['range'];
	if($range === "l55"){
		$data_query .= "and finalagg < 55";//"";
	}else{
		$data_query .= " and finalagg >=".$range;
	}
}else{
	$range = 55;
	$data_query .= " and finalagg >=".$range;
}
$data_query .=  ' and placed_in = "" order by roll_no'; 

//echo $data_query;

$student_data = mysqli_query($db, $data_query);

$range1_query = "select * from user natural join marks where roll_no like '".$ref."%' and finalagg >= 55 and placed_in = '' order by roll_no";
$range1_data = mysqli_query($db, $range1_query);
$range1_no = mysqli_num_rows($range1_data);

$range2_query = "select * from user natural join marks where roll_no like '".$ref."%' and finalagg >= 57 and placed_in = '' order by roll_no";
$range2_data = mysqli_query($db, $range2_query);
$range2_no = mysqli_num_rows($range2_data);

$range3_query = "select * from user natural join marks where roll_no like '".$ref."%' and finalagg >= 58 and placed_in = '' order by roll_no";
$range3_data = mysqli_query($db, $range3_query);
$range3_no = mysqli_num_rows($range3_data);

$range4_query = "select * from user natural join marks where roll_no like '".$ref."%' and finalagg >= 60 and placed_in = '' order by roll_no";
$range4_data = mysqli_query($db, $range4_query);
$range4_no = mysqli_num_rows($range4_data);

$range5_query = "select * from user natural join marks where roll_no like '".$ref."%' and finalagg < 55 and placed_in = '' order by roll_no";
$range5_data = mysqli_query($db, $range5_query);
$range5_no = mysqli_num_rows($range5_data);
?>
<script type="text/javascript">
	$(document).ready(function(){
	
		$("#mrange").change(function(){
		
			var choice = $(this).val();
			
			var redirect;
			switch(choice){
			
				case "55 and above":
					redirect = 55;
					break;
					
				case "57 and above":
					redirect = 57;
					break;
					
				case "58 and above":
					redirect = 58;
					break;
					
				case "60 and above":
					redirect = 60;
					break;		
				
				case "Less than 55":
					redirect = "l55";
					break;
			}
			
			window.open("http://placement.compag.in/admin/export.php?ref=<?php echo $ref;?>&range="+redirect, "_self")
		});
	});
	// <a href='http://placement.compag.in/admin/download.php?file=$filename.xlsx'>here</a>
</script>
<div id= "excel_gen">
	<br/>
	<center>
	<select id = "mrange">
		<option <?php if($range == 55) echo "selected=selected";?>>55 and above</option>
		<option <?php if($range == 57) echo "selected=selected";?>>57 and above</option>
		<option <?php if($range == 58) echo "selected=selected";?>>58 and above</option>
		<option <?php if($range == 60) echo "selected=selected";?>>60 and above</option>
		<option <?php if($range == "l55") echo "selected=selected";?>>Less than 55</option>


	</select>&nbsp;&nbsp;&nbsp;&nbsp;
	<a href='<?= baseurl?>admin/download.php?file=<?= $filename; ?>.xlsx'><?= $filename; ?>.xlsx</a>
	</center>
</div>
<hr noshade style = "border: 2px solid #CCCCCC;" />
<?php
//Script for bar chart
//seperating branch name from filename
$temp = explode("_", $filename);
$branch_name = $temp[0]
?>

 <script type="text/javascript" src="https://www.google.com/jsapi"></script>
    <script type="text/javascript">
      google.load("visualization", "1", {packages:["corechart"]});
      google.setOnLoadCallback(drawChart);
      function drawChart() {
        var data = google.visualization.arrayToDataTable([
          ['Marks Range', 'No. of students'],
          ['>=55',  <?= $range1_no; ?>],
          ['>=57',  <?= $range2_no; ?>],
          ['>=58',  <?= $range3_no; ?>],
          ['>=60',  <?= $range4_no; ?>],
          ['<55',  <?= $range5_no; ?>]       

        ]);

        var options = {
          title: '<?= $branch_name; ?> Branch Performance',
          hAxis: {title: 'Marks Range', titleTextStyle: {color: 'black'}}
        };

        var chart = new google.visualization.ColumnChart(document.getElementById('chart_div'));
        chart.draw(data, options);
      }
    </script>

     <div id="chart_div" style="width: 750px; height: 500px;"></div>
     <hr noshade style = "border: 2px solid #CCCCCC;" />
     <p class = "notify">Please put the cursor over the bar in chart to get the number of students.</p>
     <hr noshade style = "border: 2px solid #CCCCCC;" />

<?php
$count = 3;
while($row = mysqli_fetch_array($student_data)){
	
	$username = $row['fname']." ".$row['mname']." ".$row['lname'];
	
	$objPHPExcel->getActiveSheet()->SetCellValue('A'.$count, $row['roll_no']);
	$objPHPExcel->getActiveSheet()->SetCellValue('B'.$count, $username);
	$objPHPExcel->getActiveSheet()->SetCellValue('C'.$count, $row['contact_no']);
	$objPHPExcel->getActiveSheet()->SetCellValue('D'.$count, $row['dob']);
	$objPHPExcel->getActiveSheet()->SetCellValue('E'.$count, $row['fname']);
	$objPHPExcel->getActiveSheet()->SetCellValue('F'.$count, $row['mname']);
	$objPHPExcel->getActiveSheet()->SetCellValue('G'.$count, $row['lname']);
	$objPHPExcel->getActiveSheet()->SetCellValue('H'.$count, $row['email']);
	$objPHPExcel->getActiveSheet()->SetCellValue('I'.$count, $row['address']);
	
	$objPHPExcel->getActiveSheet()->SetCellValue('J'.$count, $row['sscmk']);
	$objPHPExcel->getActiveSheet()->SetCellValue('K'.$count, $row['sscout']);
	$objPHPExcel->getActiveSheet()->SetCellValue('L'.$count, $row['sscper']);
	$objPHPExcel->getActiveSheet()->SetCellValue('M'.$count, $row['sscyear']);
	
	$objPHPExcel->getActiveSheet()->SetCellValue('N'.$count, $row['hscmk']);
	$objPHPExcel->getActiveSheet()->SetCellValue('O'.$count, $row['hscout']);
	$objPHPExcel->getActiveSheet()->SetCellValue('P'.$count, $row['hscper']);
	$objPHPExcel->getActiveSheet()->SetCellValue('Q'.$count, $row['hscyear']);

	$objPHPExcel->getActiveSheet()->SetCellValue('R'.$count, $row['cetmk']);
	$objPHPExcel->getActiveSheet()->SetCellValue('S'.$count, $row['cetout']);

	$objPHPExcel->getActiveSheet()->SetCellValue('T'.$count, $row['aieeemk']);
	$objPHPExcel->getActiveSheet()->SetCellValue('U'.$count, $row['aieeeout']);	


	$objPHPExcel->getActiveSheet()->SetCellValue('V'.$count, $row['dipbranch']);
	$objPHPExcel->getActiveSheet()->SetCellValue('W'.$count, $row['dipyear']);
	
	$objPHPExcel->getActiveSheet()->SetCellValue('X'.$count, $row['dipfmk']);
	$objPHPExcel->getActiveSheet()->SetCellValue('Y'.$count, $row['dipfout']);
	$objPHPExcel->getActiveSheet()->SetCellValue('Z'.$count, $row['dipfper']);
	$objPHPExcel->getActiveSheet()->SetCellValue('AA'.$count, $row['dipaggper']);

	$objPHPExcel->getActiveSheet()->SetCellValue('AB'.$count, $row['sem1mk']);
	$objPHPExcel->getActiveSheet()->SetCellValue('AC'.$count, $row['sem1out']);
	$objPHPExcel->getActiveSheet()->SetCellValue('AD'.$count, $row['sem1per']);

	$objPHPExcel->getActiveSheet()->SetCellValue('AE'.$count, $row['sem2mk']);
	$objPHPExcel->getActiveSheet()->SetCellValue('AF'.$count, $row['sem2out']);
	$objPHPExcel->getActiveSheet()->SetCellValue('AG'.$count, $row['sem2per']);
	
	
	$objPHPExcel->getActiveSheet()->SetCellValue('AH'.$count, $row['sem3mk']);
	$objPHPExcel->getActiveSheet()->SetCellValue('AI'.$count, $row['sem3out']);
	$objPHPExcel->getActiveSheet()->SetCellValue('AJ'.$count, $row['sem3per']);
	
	
	$objPHPExcel->getActiveSheet()->SetCellValue('AK'.$count, $row['sem4mk']);
	$objPHPExcel->getActiveSheet()->SetCellValue('AL'.$count, $row['sem4out']);
	$objPHPExcel->getActiveSheet()->SetCellValue('AM'.$count, $row['sem4per']);
	
	$objPHPExcel->getActiveSheet()->SetCellValue('AN'.$count, $row['sem5mk']);
	$objPHPExcel->getActiveSheet()->SetCellValue('AO'.$count, $row['sem5out']);
	$objPHPExcel->getActiveSheet()->SetCellValue('AP'.$count, $row['sem5per']);
	
	$objPHPExcel->getActiveSheet()->SetCellValue('AQ'.$count, $row['sem6mk']);
	$objPHPExcel->getActiveSheet()->SetCellValue('AR'.$count, $row['sem6out']);
	$objPHPExcel->getActiveSheet()->SetCellValue('AS'.$count, $row['sem6per']);
	
	$objPHPExcel->getActiveSheet()->SetCellValue('AT'.$count, $row['aggmk']);
	$objPHPExcel->getActiveSheet()->SetCellValue('AU'.$count, $row['aggout']);
	$objPHPExcel->getActiveSheet()->SetCellValue('AV'.$count, $row['finalagg']);
	
	$objPHPExcel->getActiveSheet()->SetCellValue('AW'.$count, $row['livekt']);
	$objPHPExcel->getActiveSheet()->SetCellValue('AX'.$count, $row['deadkt']);

	$count ++;
}



$objPHPExcel->getActiveSheet()->setTitle('Student Data');

	
// Save Excel 2007 file

$objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
$objWriter->save(str_replace('.php', '.xlsx', '/home/compa3h9/public_html/placement/admin/files/'.$filename.'.xlsx'));


?>


</div>

<?php
	include '../includes/footer.inc.php';
?>