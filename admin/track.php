<?php
	include '../includes/header.inc.php';
	include '../includes/connect.inc.php';
	include '../includes/session.inc.php';
	include '../includes/admincheck.inc.php';
?>
	
<div class="linkcontent" style = "margin-top:40px; width: 800px;">
	
	<hr noshade style = "border: 2px solid #CCCCCC;" /><br />
		<a class="orange" style="font-size:13px;text-decoration: none;" href="track.php?ref=1">Computer</a>&nbsp;&nbsp;
		<a class="orange" style="font-size:13px;text-decoration: none;" href="track.php?ref=2">Mechanical</a>&nbsp;&nbsp;
		<a class="orange" style="font-size:13px;text-decoration: none;" href="track.php?ref=3">EXTC</a>&nbsp;&nbsp;
		<a class="orange" style="font-size:13px;text-decoration: none;" href="track.php?ref=4">Electrical</a>&nbsp;&nbsp;
		<a class="orange" style="font-size:13px;text-decoration: none;" href="track.php?ref=5">IT</a>
	<br /><br />
	<hr noshade style = "border: 2px solid #CCCCCC;" /><br />

<?php 

	// Setting update notification
  if(!empty($_SESSION['update_msg']))
  {
    echo $_SESSION['update_msg']; 
    unset($_SESSION['update_msg']);
  }

	$db = mysqli_connect(DB_HOST, DB_USER, DB_PASSWORD, DB_NAME);
	
	if(isset($_GET['ref']))
	{
		if ($_GET['ref'] >= 1 && $_GET['ref'] <= 5)
		{
			$ref = $_GET['ref'];
		}
		else
		{
			$ref = 1;
		}
	}
	else
	{
		$ref = 1;
	}
	
	$notRegisteredStudentsQuery = "select * from student where roll_no like '".$ref."%' and roll_no not in (select roll_no from user order by roll_no asc)";
	$notRegisteredStudentsData = mysqli_query($db, $notRegisteredStudentsQuery);
	$notRegisteredStudentsCount = mysqli_query($db, $notRegisteredStudentsQuery);

	//------------------------------------------------------------------------------------------------------------------------

	$notMarksEnteredStudentsQuery = "select * from user where roll_no like '".$ref."%' and user_id not in (select user_id from marks order by user_id asc)";
	$notMarksEnteredStudentsData = mysqli_query($db, $notMarksEnteredStudentsQuery);
	$notMarksEnteredStudentsCount = mysqli_query($db, $notMarksEnteredStudentsQuery);

	// Getting the count
	$remaining_count = mysqli_num_rows($notRegisteredStudentsCount);
	$marks_remaining_count = mysqli_num_rows($notMarksEnteredStudentsCount);
	$total_count_query = "select * from student where roll_no like '".$ref."%'";
	$total_count_data = mysqli_query($db, $total_count_query);
	$total_count = mysqli_num_rows($total_count_data);
	
	?>
	<!--<div id = "reg-count"><?=$remaining_count?>/<?=$total_count?> + <?=$marks_remaining_count?> (Marks not added)</div>
	<h2>Registration Remaining</h2>
	<hr noshade style = "border: 1px solid #CCCCCC;" />-->
	<table id = "record">
		<tr><th>Roll No.</th><th>Name</th><th>Email ID</th><th>Registration</th><th>Marks</th><th>action</th></tr>
		<?php
		while($row = mysqli_fetch_array($notRegisteredStudentsData))
		{	
		?>
			<tr>
				<td id = "roll_no"><?=$row['roll_no']?></td>
				<td id = "name"><?=$row['name']?></td>
				<td id = "email_id"><?=$row['email']?></td>
				<td id = "registration">
					<img src="<?php echo baseurl; ?>images/cross.png"/>
				</td>
				<td id = "marks">
					<img src="<?php echo baseurl; ?>images/cross.png"/>
				</td>
				<td><a href="<?php echo baseurl; ?>admin/editstudent.php?roll_no=<?php echo $row['roll_no']?>">Edit</a></td>
			</tr>
		<?php
			}
		?>

		<?php
		while($row = mysqli_fetch_array($notMarksEnteredStudentsData))
		{	
		?>
			<tr>
				<td id = "roll_no"><?=$row['roll_no']?></td>
				<td id = "name"><?=$row["fname"]." ".$row["lname"];?></td>
				<td id = "email"><?=$row['email']?></td>
				<td id = "registration">
					<img src="<?php echo baseurl; ?>images/correct.png"/>
				</td>
				<td id = "marks">
					<img src="<?php echo baseurl; ?>images/cross.png"/>
				</td>
				<td><a href="<?php echo baseurl; ?>admin/editstudent.php?roll_no=<?php echo $row['roll_no']?>">Edit</a></td>
			</tr>
		<?php
			}
		?>
	</table>
	
	<!--<h2>Marks Remaining</h2>
	<hr noshade style = "border: 1px solid #CCCCCC;" />
	
	<table id = "record">
		<tr>
			<th>Roll No.</th>
			<th>Name</th>
			<th>Email ID</th>
		</tr>
	</table>-->
</div>

<?php
	include '../includes/footer.inc.php';
?>