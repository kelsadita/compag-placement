<?php
include '../includes/header.inc.php';
include '../includes/connect.inc.php';
include '../includes/session.inc.php';
?>

<div class="linkcontent" style = "margin-top:40px; width: 1000px;">
	<?php
  $back_url = baseurl."admin/records.php";
  $db = mysqli_connect(DB_HOST, DB_USER, DB_PASSWORD, DB_NAME);
  if(isset($_POST['submit']))
  {
   $placed_in = $_POST['placed_in'];
   $user_id = $_POST['user_id'];
   $roll_no = $_POST['rollno'];
   $ref = $_GET['ref'];
   
   if($placed_in == "NONE"){
     
     $placed_in = "";
     
   }
   
   $query="update user set placed_in='$placed_in' where user_id=$user_id";
   if(mysqli_query($db,$query))
   {
     $_SESSION['placement_update']="Placement status for $roll_no updated successfully...";
     echo $_SESSION['placement_update'];
     header("Location: records.php?ref=$ref");
   }
   
 }
 else
 {	
  if(isset($_GET['user_id'])&&isset($_GET['roll_no'])&&isset($_GET['ref']))
  {
   $roll_no=$_GET['roll_no'];
   $user_id=$_GET['user_id'];
   $ref = $_GET['ref'];
 }
 else
 {
  echo "error";
}
$query = "select * from user where user_id=$user_id";
$data = mysqli_query($db, $query);
$row = mysqli_fetch_array($data);
$placed_in = $row['placed_in'];
}
?>
<form class="cmxform" id="form" action=<?php echo $_SERVER['PHP_SELF']."?roll_no=$roll_no&user_id=$user_id&ref=$ref"; ?> method="post">
  <hr noshade style = "border : 2px solid #CCCCCC;"/>
  <h2>Placement Update<a href="<?=$back_url;?>" class="orange" style = "float: right; font-size: 13px;text-decoration: none;">Back</a></h2>
  <hr noshade style = "border : 2px solid #CCCCCC;"/>
  <br /><br />
  <label>Roll Number</label>
  <input type="text" name="rollno" class="required number" value="<?php echo $roll_no;?>" readonly="readonly"/><br/>
  <label>Name</label>
  <input type="text" name="name" class="required" value="<?php echo $row['fname']." ".$row['lname'];?>" readonly="readonly"/><br/>
  <input type="hidden" value=<?php echo $user_id; ?> name="user_id"/>
  <label>Placed In</label>
  <select name="placed_in" id="placed">
    <option>NONE</option>
    <?php 
    $query_company = "select * from company";
    $data_company = mysqli_query($db, $query_company);
    while($row_company = mysqli_fetch_array($data_company))
    {
      ?>
      <option <?php if($placed_in == $row_company['name']) echo "SELECTED" ?>><?php echo $row_company['name'];?></option>
      <?php
    }
    ?>
  </select><br /><br />
  <br />
  <label>&nbsp;</label>
  <input type="submit" value="Submit" name="submit" class="button" /></div>
</form>

</div>

<?php
include '../includes/footer.inc.php';
?>