<?php
	include '../includes/header.inc.php';
	include '../includes/connect.inc.php';
	include '../includes/session.inc.php';
	include '../includes/admincheck.inc.php';
?>
	
<div class="linkcontent" style = "margin-top:40px; width: 1000px;">
	
	<hr noshade style = "border: 2px solid #CCCCCC;" /><br />
		<a class="orange" style="font-size:13px;text-decoration: none;" href="records.php?ref=1">Computer</a>&nbsp;&nbsp;
		<a class="orange" style="font-size:13px;text-decoration: none;" href="records.php?ref=2">Mechanical</a>&nbsp;&nbsp;
		<a class="orange" style="font-size:13px;text-decoration: none;" href="records.php?ref=3">EXTC</a>&nbsp;&nbsp;
		<a class="orange" style="font-size:13px;text-decoration: none;" href="records.php?ref=4">Electrical</a>&nbsp;&nbsp;
		<a class="orange" style="font-size:13px;text-decoration: none;" href="records.php?ref=5">IT</a>
	<br /><br />

<?php 
	$db = mysqli_connect(DB_HOST, DB_USER, DB_PASSWORD, DB_NAME);
	
	if(isset($_GET['ref'])){
		if ($_GET['ref'] >= 1 && $_GET['ref'] <= 5){
			$ref = $_GET['ref'];
		}else{
			$ref = 1;
		}
	}else{
		$ref = 1;
	}
	
	$query = "select * from user where roll_no like '".$ref."%' order by roll_no";
	$data = mysqli_query($db, $query);
	while($row = mysqli_fetch_array($data)){
		
		$user_id = $row['user_id'];
		
		$marks_query = "select * from marks where user_id = $user_id";
		$marks_data = mysqli_query($db, $marks_query);
		$marks_row = mysqli_fetch_array($marks_data);
		if(isset($_SESSION['update']))
		{
			 unset($_SESSION['update']);
			 echo '<p class="notify">Profile has been updated successfully.</p>';
		}
		else if(isset($_SESSION['placement_update']))
		{
			?>
			<p class='notify'><?php echo $_SESSION['placement_update']; ?></p>
			<?php
			unset($_SESSION['placement_update']);
			
		}
?>		
		<hr noshade style = "border: 2px solid #CCCCCC;" />
		<center><h2 id="<?php echo  $row['roll_no']?>"><?= $row['roll_no'];
		if($row['placed_in']!="") 
		{
			if($row['placed_in']!="Not in the process")
			{
				echo "<font color='red'>(Placed In : ".$row['placed_in'].")
				</font>";
			}
			else
			{
				echo "<font color='red'>(Excluded from the Placement Process)
				</font>";
			}
		}
		?>
		<a href="<?php echo baseurl;?>admin/edit_placement.php?user_id=<?= $user_id;?>&roll_no=<?= $row['roll_no'];?>&ref=<?= $ref;?>" class="orange" style="float: right;font-size:13px;text-decoration:none;">Placement Status</a></h2></center>
		
		<table style ="border-left: 1px solid #CCCCCC; border-right: 1px solid #CCCCCC; width: 100%">
		<tr>
			<td style = "width: 40%; border-right: 1px solid #CCCCCC;">
				<hr noshade style = "border: 2px solid #CCCCCC;" />
				<h2>Personal Details<a href="<?php echo baseurl;?>profile/edit_profile.php?user_id=<?= $user_id;?>&roll_no=<?= $row['roll_no'];?>&ref=<?= $ref;?>" class="orange" style="float: right;font-size:13px;text-decoration:none;">Edit</a></h2>
				<hr noshade style = "border: 2px solid #CCCCCC;" />
				<table cellpadding = "7">
	    			<tr><td><b class = "vpelements">First&nbsp;Name</b></td><td><?php echo $row['fname'];?></td></tr>
	    			<tr><td><b class = "vpelements">Middle&nbsp;Name</b></td><td><?php echo $row['mname'];?></td></tr>
	    			<tr><td><b class = "vpelements">Last&nbsp;Name</b></td><td><?php echo $row['lname'];?></td></tr>
	    			<tr><td><b class = "vpelements">Gender</b></td><td><?php echo $row['gender'];?></td></tr>	    			
	    			<tr><td><b class = "vpelements">Roll&nbsp;No.</b></td><td><?php echo $row['roll_no'];?></td></tr>
	    			<tr><td><b class = "vpelements">Branch</b></td><td><?php echo $row['branch'];?></td></tr>
	    			<tr><td><b class = "vpelements">Contact&nbsp;No.</b></td><td><?php echo $row['contact_no'];?></td></tr>
	    			<tr><td><b class = "vpelements">Birth Date</b></td><td><?php echo $row['dob'];?></td></tr>
	    			<tr><td><b class = "vpelements">Email</b></td><td><?php echo $row['email'];?></td></tr>
	    			<tr><td><b class = "vpelements">Address</b></td><td><?php echo $row['address'];?></td></tr>
				</table>
			</td>
			<td style = "width: 60%;">
				<hr noshade style = "border: 2px solid #CCCCCC;" />
				<h2>Marks Details<a href="<?php echo baseurl;?>profile/edit_form.php?user_id=<?= $user_id;?>&bref=<?= $ref;?>" class="orange" style="float: right;font-size:13px;text-decoration:none;">Edit</a></h2>
				<hr noshade style = "border: 2px solid #CCCCCC;" />
				<br/><br/>	
				<!--Entering SSC data-->
				<p class = "heading">SSC Information</p>
				<table cellpadding = "7" style = "width: 484px;">
		    		<tr>
		    			<td><b class = "vpelements">SSC Year</b></td><td><?= $marks_row['sscyear'];?></td>
		    		</tr>
		    		<tr>
						<td><b class = "vpelements">SSC Marks</b></td><td><?= $marks_row['sscmk'];?>&nbsp;/&nbsp;<?= $marks_row['sscout']; ?></td>
		    			<td><b class = "vpelements">percentage</b></td><td><?= $marks_row['sscper']; ?></td>
		    		</tr>
				</table>

				<!--Displaying CET/AIEEE Data-->
				<?php

				if($marks_row['cetmk']!=0)
				{

					?>

						<p class = "heading">CET Score</p>
						<table cellpadding = "7" style = "width: 484px;">
				    		<tr>
								<td><b class = "vpelements">CET Marks</b></td><td><?= $marks_row['cetmk'];?>&nbsp;/&nbsp;<?= $marks_row['cetout']; ?></td>
				    		</tr>
						</table>
					<?php
				}

				if($marks_row['aieeemk']!=0)
				{
					?>
						<p class = "heading">AIEEE Score</p>
						<table cellpadding = "7" style = "width: 484px;">
				    		<tr>
								<td><b class = "vpelements">AIEEE Marks</b></td><td><?= $marks_row['aieeemk'];?>&nbsp;/&nbsp;<?= $marks_row['aieeeout']; ?></td>
				    		</tr>
						</table>
					<?php
				}

				?>

				<!--Entering diploma data -->
				<?php if($marks_row['dipfmk'] != 0 && $marks_row['hscmk'] == 0){?>	
	
				<p class = "heading">Diploma Information</p>
				<table cellpadding = "4"  style = "width: 484px;">
	    			<tr><td><b class = "vpelements">Diploma Year</b></td><td><?= $marks_row['dipyear'];?></td></tr>
	    			<tr><td><b class = "vpelements">Diploma Branch</b></td><td><?= $marks_row['dipbranch'];?></td></tr>
	    			<tr>
				<td><b class = "vpelements">Final Year Marks</b></td><td><?= $marks_row['dipfmk'];?>&nbsp;/&nbsp;<?= $marks_row['dipfout']; ?></td>
	    			<td><b class = "vpelements">percentage</b></td><td><?= $marks_row['dipfper']; ?></td>
	    			</tr>
	    			<tr>
				<td><b class = "vpelements">Aggregate Marks</b></td><td><?= $marks_row['dipaggmk'];?>&nbsp;/&nbsp;<?= $marks_row['dipaggout']; ?></td>
	    			<td><b class = "vpelements">percentage</b></td><td><?= $marks_row['dipaggper']; ?></td>
	    			</tr>
				</table>
	
	
				<?php 
				}

				elseif($marks_row['dipfmk'] == 0 && $marks_row['hscmk'] != 0)
				{ 

				?>
				<!--Entering HSC data-->
				<p class = "heading">HSC Information</p>
				<table cellpadding = "7"  style = "width: 484px;">
	    			<tr><td><b class = "vpelements">HSC Year</b></td><td><?= $marks_row['hscyear'];?></td></tr>
	    			<tr>
					<td><b class = "vpelements">HSC Marks</b></td><td><?= $marks_row['hscmk'];?>&nbsp;/&nbsp;<?= $marks_row['hscout']; ?></td>
	    				<td><b class = "vpelements">percentage</b></td><td><?= $marks_row['hscper']; ?></td>
	    			</tr>
				</table>

				
				
				<?php 

				}

				elseif($marks_row['dipfmk'] != 0 && $marks_row['hscmk'] != 0)
				{
					?>

					<p class = "heading">HSC Information</p>
					<table cellpadding = "7"  style = "width: 484px;">
		    			<tr><td><b class = "vpelements">HSC Year</b></td><td><?= $marks_row['hscyear'];?></td></tr>
		    			<tr>
						<td><b class = "vpelements">HSC Marks</b></td><td><?= $marks_row['hscmk'];?>&nbsp;/&nbsp;<?= $marks_row['hscout']; ?></td>
		    				<td><b class = "vpelements">percentage</b></td><td><?= $marks_row['hscper']; ?></td>
		    			</tr>
					</table>

					<p class = "heading">Diploma Information</p>
					<table cellpadding = "4"  style = "width: 484px;">
		    			<tr><td><b class = "vpelements">Diploma Year</b></td><td><?= $marks_row['dipyear'];?></td></tr>
		    			<tr><td><b class = "vpelements">Diploma Branch</b></td><td><?= $marks_row['dipbranch'];?></td></tr>
		    			<tr>
					<td><b class = "vpelements">Final Year Marks</b></td><td><?= $marks_row['dipfmk'];?>&nbsp;/&nbsp;<?= $marks_row['dipfout']; ?></td>
		    			<td><b class = "vpelements">percentage</b></td><td><?= $marks_row['dipfper']; ?></td>
		    			</tr>
		    			<tr>
					<td><b class = "vpelements">Aggregate Marks</b></td><td><?= $marks_row['dipaggmk'];?>&nbsp;/&nbsp;<?= $marks_row['dipaggout']; ?></td>
		    			<td><b class = "vpelements">percentage</b></td><td><?= $marks_row['dipaggper']; ?></td>
		    			</tr>
					</table>

					<?php

				} 


				?>



				<p class = "heading">Engineering Degree Information</p>
				<table cellpadding = "7" >
				<?php if($marks_row['dipfmk'] == 0){ ?>
				
				<tr>
					<td><b class = "vpelements">Semester 1</b></td><td><?= $marks_row['sem1mk'];?>&nbsp;/&nbsp;<?= $marks_row['sem1out']; ?></td>
	    				<td><b class = "vpelements">percentage</b></td><td><?= $marks_row['sem1per']; ?></td>
				</tr>
				<tr>
	    				<td><b class = "vpelements">Semester 2</b></td><td><?= $marks_row['sem2mk'];?>&nbsp;/&nbsp;<?= $marks_row['sem2out']; ?></td>
	   	 			<td><b class = "vpelements">percentage</b></td><td><?= $marks_row['sem2per']; ?></td>
				</tr>
				
				<?php } ?>
		
				<tr>
	    				<td><b class = "vpelements">Semester 3</b></td><td><?= $marks_row['sem3mk'];?>&nbsp;/&nbsp;<?= $marks_row['sem3out']; ?></td>
	    				<td><b class = "vpelements">percentage</b></td><td><?= $marks_row['sem3per']; ?></td>
				</tr>
				<tr>
	    				<td><b class = "vpelements">Semester 4</b></td><td><?= $marks_row['sem4mk'];?>&nbsp;/&nbsp;<?= $marks_row['sem4out']; ?></td>
	    				<td><b class = "vpelements">percentage</b></td><td><?= $marks_row['sem4per']; ?></td>
				</tr>
				<tr>
	    				<td><b class = "vpelements">Semester 5</b></td><td><?= $marks_row['sem5mk'];?>&nbsp;/&nbsp;<?= $marks_row['sem5out']; ?></td>
	    				<td><b class = "vpelements">percentage</b></td><td><?= $marks_row['sem5per']; ?></td>
				</tr>
				<tr>
	    				<td><b class = "vpelements">Semester 6</b></td><td><?= $marks_row['sem6mk'];?>&nbsp;/&nbsp;<?= $marks_row['sem6out']; ?></td>
	    				<td><b class = "vpelements">percentage</b></td><td><?= $marks_row['sem6per']; ?></td>
				</tr>
				<tr>
	    				<td><b class = "vpelements">Aggregate </b></td><td><?= $marks_row['aggmk'];?>&nbsp;/&nbsp;<?= $marks_row['aggout']; ?></td>
	    				<td><b class = "vpelements">percentage</b></td><td><?= $marks_row['finalagg']; ?></td>
				</tr>
    				</table>

				<?php if($marks_row['livekt'] != 0 || $marks_row['deadkt'] != 0){ ?>

				<p class = "heading">KT Information</p>
				
				<table cellpadding = "7">
	    				<tr><td><b class = "vpelements">Live KT</b></td><td><?= $marks_row['livekt'];?></td></tr>
	    				<tr><td><b class = "vpelements">Dead KT</b></td><td><?= $marks_row['deadkt'];?></td></tr>
				</table>
				<?}?>
			</td>
		</tr>
		</table>
<?php
		
		
	}
?>

   

</div>

<?php
	include '../includes/footer.inc.php';
?>